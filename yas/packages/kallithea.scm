(define-module (yas packages kallithea)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:select (alist-delete)))


(define-public python-paginate
  (package
    (name "python-paginate")
    (version "0.5.6")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "paginate" version))
       (sha256
        (base32 "0bdgg269x4pihancwnwl39m7djgqv7yh93b4w6kpg09rm6v0fq2y"))))
    (build-system python-build-system)
    (home-page "https://github.com/Signum/paginate")
    (synopsis "Divides large result sets into pages for easier browsing")
    (description "Divides large result sets into pages for easier browsing")
    (license license:expat)))

(define-public python-paginate-sqlalchemy
  (package
    (name "python-paginate-sqlalchemy")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "paginate-sqlalchemy" version))
       (sha256
        (base32 "0nwv6rnfgrkpb3gn70v3gq9wh8j2l860sa5ibi2m4bsz9avlkqwy"))))
    (build-system python-build-system)
    (propagated-inputs
     `(("python-paginate" ,python-paginate)
       ("python-sqlalchemy" ,python-sqlalchemy)))
    (home-page "https://github.com/Pylons/paginate_sqlalchemy")
    (synopsis "Extension to paginate.Page that supports SQLAlchemy queries")
    (description "Extension to paginate.Page that supports SQLAlchemy queries")
    (license license:expat)))

(define-public python-ipaddr
  (package
    (name "python-ipaddr")
    (version "2.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "ipaddr" version))
       (sha256
        (base32 "1ml8r8z3f0mnn381qs1snbffa920i9ycp6mm2am1d3aqczkdz4j0"))))
    (build-system python-build-system)
    (home-page "https://github.com/google/ipaddr-py")
    (synopsis "Google's IP address manipulation library")
    (description "Google's IP address manipulation library")
    (license license:asl2.0)))

;;; (define-public python-routes
;;;   (package
;;;     (name "python-routes")
;;;     (version "2.5.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Routes" version))
;;;        (sha256
;;;         (base32 "0lyh4m8qcawmq568nqixfga81yfa4lyhraa53aqbl32zl5cn8d5n"))))
;;;     (build-system python-build-system)
;;;     (arguments `(#:tests? #f))
;;;     (native-inputs
;;;      `(("python-coverage" ,python-coverage)))
;;;     (propagated-inputs
;;;      `(("python-repoze.lru" ,python-repoze.lru)
;;;        ("python-six" ,python-six)))
;;;     (home-page "https://routes.readthedocs.io/")
;;;     (synopsis "Routing Recognition and Generation Tools")
;;;     (description "Routing Recognition and Generation Tools")
;;;     (license license:expat)))
;;; 
;;; (define-public python-urlobject
;;;   (package
;;;     (name "python-urlobject")
;;;     (version "2.4.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "URLObject" version))
;;;        (sha256
;;;         (base32 "1ahc8ficzfvr2avln71immfh4ls0zyv6cdaa5xmkdj5rd87f5cj7"))))
;;;     (build-system python-build-system)
;;;     (home-page "http://github.com/zacharyvoase/urlobject")
;;;     (synopsis "A utility class for manipulating URLs.")
;;;     (description "A utility class for manipulating URLs.")
;;;     (license #f)))
;;; 
;;; (define-public python-webhelpers2
;;;   (package
;;;     (name "python-webhelpers2")
;;;     (version "2.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "WebHelpers2" version))
;;;        (sha256
;;;         (base32 "0aphva1qmxh83n01p53f5fd43m4srzbnfbz5ajvbx9aj2aipwmcs"))))
;;;     (build-system python-build-system)
;;;     (arguments `(#:tests? #f))
;;;     (propagated-inputs
;;;      `(("python-markupsafe" ,python-markupsafe) ("python-six" ,python-six)))
;;;     (home-page "https://webhelpers2.readthedocs.org/en/latest/")
;;;     (synopsis "WebHelpers2")
;;;     (description "WebHelpers2")
;;;     (license #f)))
;;; 
;;; 
;;; (define-public python-tgext.routes
;;;   (package
;;;     (name "python-tgext.routes")
;;;     (version "0.2.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "tgext.routes" version))
;;;        (sha256
;;;         (base32 "13f2p7h4b6fhdlg0xyd9qv1mq1aqvcki7lndx70hwng479va1ip7"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-routes" ,python-routes)
;;;        ("python-turbogears2" ,python-turbogears2)))
;;;     (native-inputs
;;;      `(("python-pytest" ,python-pytest)
;;;        ("python-webtest" ,python-webtest)))
;;;     (home-page "https://github.com/TurboGears/tgext.routes")
;;;     (synopsis "Routes based dispatching for TurboGears2")
;;;     (description "Routes based dispatching for TurboGears2")
;;;     (license license:expat)))
;;; 
;;; (define-public python-formencode
;;;   (package
;;;     (name "python-formencode")
;;;     (version "2.0.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "FormEncode" version))
;;;        (sha256
;;;         (base32 "0d80f4zvyj7ivsxkm4jxg0q3ih04054pd2xfbffq6mr55h8p8acg"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-six" ,python-six)))
;;;     (arguments `(#:tests? #f))
;;;     (native-inputs
;;;      `(("python-dnspython" ,python-dnspython)
;;;        ("python-setuptools-scm-git-archive" ,python-setuptools-scm-git-archive)
;;;        ("python-pycountry" ,python-pycountry)
;;;        ("python-pytest" ,python-pytest)))
;;;     (home-page "http://formencode.org")
;;;     (synopsis "HTML form validation, generation, and conversion package")
;;;     (description "HTML form validation, generation, and conversion package")
;;;     (license license:expat)))
;;; 
;;; (define-public python-toscawidgets
;;;   (package
;;;     (name "python-toscawidgets")
;;;     (version "0.9.12")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "ToscaWidgets" version))
;;;        (sha256
;;;         (base32 "10w4j6rl1131dji7avxs30j8n2nia4w3kjri2bwpwrsm9brbmnpx"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-webob" ,python-webob)))
;;;     (home-page "http://toscawidgets.org/")
;;;     (synopsis "Web widget creation toolkit based on TurboGears widgets")
;;;     (description "Web widget creation toolkit based on TurboGears widgets")
;;;     (license license:expat)))
;;; 
;;; (define-public python-ming
;;;   (package
;;;     (name "python-ming")
;;;     (version "0.11.2")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Ming" version))
;;;        (sha256
;;;         (base32 "1l96ldmdz1y16y5hnzw2hqp65mi1qzw1f2x6a6y851fqb7lm5dxv"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-pymongo" ,python-pymongo)
;;;        ("python-pytz" ,python-pytz)
;;;        ("python-six" ,python-six)))
;;;     (home-page "https://github.com/TurboGears/Ming")
;;;     (synopsis "Bringing order to Mongo since 2009")
;;;     (description "Bringing order to Mongo since 2009")
;;;     (license license:expat)))
;;; 
;;; (define-public python-ordereddict
;;;   (package
;;;     (name "python-ordereddict")
;;;     (version "1.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "ordereddict" version))
;;;        (sha256
;;;         (base32 "07qvy11nvgxpzarrni3wrww3vpc9yafgi2bch4j2vvvc42nb8d8w"))))
;;;     (build-system python-build-system)
;;;     (home-page "UNKNOWN")
;;;     (synopsis
;;;      "A drop-in substitute for Py2.7's new collections.OrderedDict that works in Python 2.4-2.6.")
;;;     (description
;;;      "A drop-in substitute for Py2.7's new collections.OrderedDict that works in Python 2.4-2.6.")
;;;     (license #f)))
;;; 
;;; 
;;; 
;;; (define-public python-beaker
;;;   (package
;;;     (name "python-beaker")
;;;     (version "1.11.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Beaker" version))
;;;        (sha256
;;;         (base32 "0zhqdl3m89rqc6w0rkkablgb8f83ny69z8rf90xbxqvy082iqpdd"))))
;;;     (build-system python-build-system)
;;;     (arguments `(#:tests? #f))
;;;     (native-inputs
;;;      `(("python-coverage" ,python-coverage)
;;;        ("python-cryptography" ,python-cryptography)
;;;        ("python-memcached" ,python-memcached)
;;;        ("python-mock" ,python-mock)
;;;        ("python-nose" ,python-nose)
;;;        ("python-pycryptodome" ,python-pycryptodome)
;;;                                         ; ("python-pylibmc" ,python-pylibmc)
;;;        ("python-pymongo" ,python-pymongo)
;;;        ("python-redis" ,python-redis)
;;;        ("python-sqlalchemy" ,python-sqlalchemy)
;;;        ("python-webtest" ,python-webtest)))
;;;     (home-page "https://beaker.readthedocs.io/")
;;;     (synopsis "A Session and Caching library with WSGI Middleware")
;;;     (description "A Session and Caching library with WSGI Middleware")
;;;     (license license:bsd-3)))
;;; 
;;; 
;;; (define-public python-nine
;;;   (package
;;;     (name "python-nine")
;;;     (version "1.1.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "nine" version))
;;;        (sha256
;;;         (base32 "1givimwypxkvsgckhr4laz4k6h5gsb0mghm9bk93f5il4rinpag8"))))
;;;     (build-system python-build-system)
;;;     (home-page "https://github.com/nandoflorestan/nine")
;;;     (synopsis "Python 2 / 3 compatibility, like six, but favouring Python 3")
;;;     (description
;;;      "Python 2 / 3 compatibility, like six, but favouring Python 3")
;;;     (license license:public-domain)))
;;; 
;;; (define-public python-kajiki
;;;   (package
;;;     (name "python-kajiki")
;;;     (version "0.8.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "kajiki" version))
;;;        (sha256
;;;         (base32 "1ncacj7nrkzx51a9k5446imxgg3sibspb955pnbzmangnlrz18f7"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-nine" ,python-nine)))
;;;     (native-inputs
;;;      `(("python-babel" ,python-babel) ("python-pytest" ,python-pytest)))
;;;     (home-page "https://github.com/jackrosenthal/kajiki")
;;;     (synopsis
;;;      "Fast XML-based template engine with Genshi syntax and Jinja blocks")
;;;     (description
;;;      "Fast XML-based template engine with Genshi syntax and Jinja blocks")
;;;     (license license:expat)))
;;; 
;;; 
;;; (define-public python-sieve
;;;   (package
;;;     (name "python-sieve")
;;;     (version "0.1.9")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "sieve" version))
;;;        (sha256
;;;         (base32 "06nm5lxj97p3lglf53r0j0av2nmbg24wnfjywg4zj7xdkksdi12c"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-lxml" ,python-lxml)
;;;        ("python-markupsafe" ,python-markupsafe)
;;;        ("python-six" ,python-six)))
;;;     (home-page "http://github.com/ralphbean/sieve")
;;;     (synopsis "XML Comparison Utils")
;;;     (description "XML Comparison Utils")
;;;     (license license:expat)))
;;; 
;;; (define-public python-speaklater
;;;   (package
;;;     (name "python-speaklater")
;;;     (version "1.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "speaklater" version))
;;;        (sha256
;;;         (base32 "1ab5dbfzzgz6cnz4xlwx79gz83id4bhiw67k1cgqrlzfs0va7zjr"))))
;;;     (build-system python-build-system)
;;;     (home-page "http://github.com/mitsuhiko/speaklater")
;;;     (synopsis
;;;      "implements a lazy string for python useful for use with gettext")
;;;     (description
;;;      "implements a lazy string for python useful for use with gettext")
;;;     (license #f)))
;;; 
;;; (define-public python-zconfig
;;;   (package
;;;     (name "python-zconfig")
;;;     (version "3.6.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "ZConfig" version))
;;;        (sha256
;;;         (base32 "1r8n1crrh7i0rcm5wckz9ka3g3vhrfr3bbfcgrs9amrkmsh9b3m2"))))
;;;     (build-system python-build-system)
;;;     (native-inputs
;;;      `(("python-docutils" ,python-docutils)
;;;        ("python-manuel" ,python-manuel)
;;;        ("python-zope.exceptions" ,python-zope.exceptions)
;;;        ("python-zope.testrunner" ,python-zope.testrunner)))
;;;     (home-page "https://github.com/zopefoundation/ZConfig/")
;;;     (synopsis "Structured Configuration Library")
;;;     (description "Structured Configuration Library")
;;;     (license #f)))
;;; 
;;; (define-public python-pytest-pythonpath
;;;   (package
;;;     (name "python-pytest-pythonpath")
;;;     (version "0.7.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "pytest-pythonpath" version))
;;;        (sha256
;;;         (base32 "0qhxh0z2b3p52v3i0za9mrmjnb1nlvvyi2g23rf88b3xrrm59z33"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-pytest" ,python-pytest)))
;;;     (home-page "https://github.com/bigsassy/pytest-pythonpath")
;;;     (synopsis
;;;      "pytest plugin for adding to the PYTHONPATH from command line or configs.")
;;;     (description
;;;      "pytest plugin for adding to the PYTHONPATH from command line or configs.")
;;;     (license license:expat)))
;;; 
;;; (define-public python-logbook
;;;   (package
;;;     (name "python-logbook")
;;;     (version "1.5.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Logbook" version))
;;;        (sha256
;;;         (base32 "1s1gyfw621vid7qqvhddq6c3z2895ci4lq3g0r1swvpml2nm9x36"))))
;;;     (build-system python-build-system)
;;;     (native-inputs
;;;      `(("python-cython" ,python-cython)
;;;        ("python-mock" ,python-mock)
;;;        ("python-pytest" ,python-pytest)
;;;        ("python-pytest-cov" ,python-pytest-cov)))
;;;     (home-page "http://logbook.pocoo.org/")
;;;     (synopsis "A logging replacement for Python")
;;;     (description "A logging replacement for Python")
;;;     (license license:bsd-3)))
;;; 
;;; (define-public python-exam
;;;   (package
;;;     (name "python-exam")
;;;     (version "0.10.6")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "exam" version))
;;;        (sha256
;;;         (base32 "128pw5rny5ilk5g80cvx52rpnb187ga5nn5h44kjjyqwpiza0b8c"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-mock" ,python-mock)))
;;;     (native-inputs `(("python-nose" ,python-nose)))
;;;     (home-page "https://github.com/fluxx/exam")
;;;     (synopsis "Helpers for better testing.")
;;;     (description "Helpers for better testing.")
;;;     (license license:expat)))
;;; 
;;; (define-public python-raven
;;;   (package
;;;     (name "python-raven")
;;;     (version "6.10.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "raven" version))
;;;        (sha256
;;;         (base32 "0m5fiwyc5g4pf6v1svx11mypj85hx569hbj74z4ag4r4z9pdx9iz"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-contextlib2" ,python-contextlib2)))
;;;     (native-inputs
;;;      `(("python-aiohttp" ,python-aiohttp)
;;;        ("python-anyjson" ,python-anyjson)
;;;        ("python-blinker" ,python-blinker)
;;;        ("python-bottle" ,python-bottle)
;;;        ("python-celery" ,python-celery)
;;;        ("python-coverage" ,python-coverage)
;;;        ("python-exam" ,python-exam)
;;;        ("python-flake8" ,python-flake8)
;;;        ("python-flask" ,python-flask)
;;;        ("python-flask-login" ,python-flask-login)
;;;        ("python-logbook" ,python-logbook)
;;;        ("python-mock" ,python-mock)
;;;        ("python-nose" ,python-nose)
;;;        ("python-pytest" ,python-pytest)
;;;        ("python-pytest-cov" ,python-pytest-cov)
;;;        ("python-pytest-flake8" ,python-pytest-flake8)
;;;        ("python-pytest-pythonpath" ,python-pytest-pythonpath)
;;;        ("python-pytest-timeout" ,python-pytest-timeout)
;;;        ("python-pytest-xdist" ,python-pytest-xdist)
;;;        ("python-pytz" ,python-pytz)
;;;        ("python-requests" ,python-requests)
;;;        ("python-sanic" ,python-sanic)
;;;        ("python-tornado" ,python-tornado)
;;;        ("python-tox" ,python-tox)
;;;        ("python-webob" ,python-webob)
;;;        ("python-webtest" ,python-webtest)
;;;        ("python-wheel" ,python-wheel)
;;;        ("python-zconfig" ,python-zconfig)))
;;;     (home-page "https://github.com/getsentry/raven-python")
;;;     (synopsis "Raven is a client for Sentry (https://getsentry.com)")
;;;     (description "Raven is a client for Sentry (https://getsentry.com)")
;;;     (license license:bsd-3)))
;;; 
;;; (define-public python-repoze.who.plugins.sa
;;;   (package
;;;     (name "python-repoze.who.plugins.sa")
;;;     (version "1.0.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "repoze.who.plugins.sa" version))
;;;        (sha256
;;;         (base32 "1kc3ccqiyfd4v3ck1q1n2q3nkcx1rckbbyna9rc11zkai3wh9bp8"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-repoze.who" ,python-repoze.who)
;;;        ("python-sqlalchemy" ,python-sqlalchemy)))
;;;     (home-page "http://code.gustavonarea.net/repoze.who.plugins.sa/")
;;;     (synopsis "The repoze.who SQLAlchemy plugin")
;;;     (description "The repoze.who SQLAlchemy plugin")
;;;     (license #f)))
;;; 
;;; (define-public python-repoze.who
;;;   (package
;;;     (name "python-repoze.who")
;;;     (version "2.4")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "repoze.who" version))
;;;        (sha256
;;;         (base32 "19llg87iq2ra8m72150hmgpipfcd05dyfyq3nh1mrsy8wc6lb5yg"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(
;;;        ("python-webob" ,python-webob)
;;;        ("python-zope.interface" ,python-zope.interface)))
;;;     (native-inputs
;;;      `(("python-coverage" ,python-coverage)
;;;        ("python-nose" ,python-nose)
;;;        ("python-webob" ,python-webob)
;;;        ("python-zope.interface" ,python-zope.interface)))
;;;     (home-page "http://www.repoze.org")
;;;     (synopsis
;;;      "repoze.who is an identification and authentication framework for WSGI.")
;;;     (description
;;;      "repoze.who is an identification and authentication framework for WSGI.")
;;;     (license #f)))
;;; 
;;; (define-public python-zope.testing
;;;   (package
;;;     (name "python-zope.testing")
;;;     (version "4.9")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.testing" version))
;;;        (sha256
;;;         (base32 "0kmm7h488ycn24fsbnfhwqkzc2w0p38gb4zf2drm97dglx3vhp27"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `())
;;;     (native-inputs `(("python-zope.testrunner" ,python-zope.testrunner)))
;;;     (home-page "https://github.com/zopefoundation/zope.testing")
;;;     (synopsis "Zope testing helpers")
;;;     (description "Zope testing helpers")
;;;     (license #f)))
;;; 
;;; (define-public python-zope.exceptions
;;;   (package
;;;     (name "python-zope.exceptions")
;;;     (version "4.4")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.exceptions" version))
;;;        (sha256
;;;         (base32 "1nkgfwawswmyc6i0b8g3ymvja4mb507m8yhid8s4rbxq3dmqhwhd"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(
;;;        ("python-zope.interface" ,python-zope.interface)))
;;;     (native-inputs `(("python-zope.testrunner" ,python-zope.testrunner)))
;;;     (home-page "https://github.com/zopefoundation/zope.exceptions")
;;;     (synopsis "Zope Exceptions")
;;;     (description "Zope Exceptions")
;;;     (license #f)))
;;; 
;;; (define-public python-zope.testrunner
;;;   (package
;;;     (name "python-zope.testrunner")
;;;     (version "5.3.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.testrunner" version))
;;;        (sha256
;;;         (base32 "03qbnxdm27wckgd7sj4l4wc6vnv3a09lfd1q26h3ww1h31ivm99r"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-six" ,python-six)
;;;        ("python-zope.exceptions" ,python-zope.exceptions)
;;;        ("python-zope.interface" ,python-zope.interface)))
;;;     (native-inputs `(("python-zope.testing" ,python-zope.testing)))
;;;     (home-page "https://github.com/zopefoundation/zope.testrunner")
;;;     (synopsis "Zope testrunner script.")
;;;     (description "Zope testrunner script.")
;;;     (license #f)))
;;; 
;;; (define-public python-zope.event
;;;   (package
;;;     (name "python-zope.event")
;;;     (version "4.5.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.event" version))
;;;        (sha256
;;;         (base32 "0c0kvfz6yazw98n0dqpp6fj6xhdn3mw1k26a6z7rl4cvbdzm2xjy"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `())
;;;     (native-inputs `(("python-zope.testrunner" ,python-zope.testrunner)))
;;;     (home-page "https://github.com/zopefoundation/zope.event")
;;;     (synopsis "Very basic event publishing system")
;;;     (description "Very basic event publishing system")
;;;     (license #f)))
;;; ;;; 
;;; (define-public python-zope.interface
;;;   (package
;;;     (name "python-zope.interface")
;;;     (version "5.4.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.interface" version))
;;;        (sha256
;;;         (base32 "03jsiad578392pfmxa1ihkmvdh2q3dcwqy1vv240jgzc1x9mzfjx"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `())
;;;     (native-inputs
;;;      `(("python-coverage" ,python-coverage)
;;;        ("python-zope.event" ,python-zope.event)
;;;        ("python-zope.testing" ,python-zope.testing)))
;;;     (home-page "https://github.com/zopefoundation/zope.interface")
;;;     (synopsis "Interfaces for Python")
;;;     (description "Interfaces for Python")
;;;     (license #f)))
;;; 
;;; (define-public python-zope.sqlalchemy
;;;   (package
;;;     (name "python-zope.sqlalchemy")
;;;     (version "1.6")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "zope.sqlalchemy" version))
;;;        (sha256
;;;         (base32 "1azm2awl2ra10xl6wps3yvy14jk2rpzvsyfsb9cncm97aydbwlww"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-sqlalchemy" ,python-sqlalchemy)
;;;        ("python-transaction" ,python-transaction)
;;;        ("python-zope.interface" ,python-zope.interface)))
;;;     (native-inputs `(("python-zope.testing" ,python-zope.testing)))
;;;     (home-page "https://github.com/zopefoundation/zope.sqlalchemy")
;;;     (synopsis "Minimal Zope/SQLAlchemy transaction integration")
;;;     (description "Minimal Zope/SQLAlchemy transaction integration")
;;;     (license #f)))
;;; 
(define-public python-repoze.lru
  (package
    (name "python-repoze.lru")
    (version "0.7")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "repoze.lru" version))
       (sha256
        (base32 "0xzz1aw2smy8hdszrq8yhnklx6w1r1mf55061kalw3iq35gafa84"))))
    (build-system python-build-system)
    (native-inputs
     `(("python-coverage" ,python-coverage)
       ("python-nose" ,python-nose)))
    (home-page "https://pypi.org/project/repoze.lru/")
    (synopsis "tiny LRU cache implementation and decorator")
    (description "A tiny LRU cache implementation and decorator")
    (license license:repoze)))
;;; 
;;; (define-public python-crank
;;;   (package
;;;     (name "python-crank")
;;;     (version "0.8.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "crank" version))
;;;        (sha256
;;;         (base32 "0zvxj2sw3jwihsqs1jrhq3ld2y9v7cabs0ihfd3vw9wxdsmr8ay2"))))
;;;     (build-system python-build-system)
;;;     (native-inputs `(("python-webob" ,python-webob)))
;;;     (home-page "https://github.com/TurboGears/crank")
;;;     (synopsis
;;;      "Generalization of dispatch mechanism for use across frameworks.")
;;;     (description
;;;      "Generalization of dispatch mechanism for use across frameworks.")
;;;     (license license:expat)))
;;; 
;;; (define-public python-turbogears2
;;;   (package
;;;     (name "python-turbogears2")
;;;     (version "2.4.3")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "TurboGears2" version))
;;;        (sha256
;;;         (base32 "1k603g4djmkq242gmhvvf0wwb156qwy6frla20cs8iai6cvacgfr"))))
;;;     (build-system python-build-system)
;;;     (arguments `(#:tests? #f))
;;;     (propagated-inputs
;;;      `(("python-crank" ,python-crank)
;;;        ("python-markupsafe" ,python-markupsafe)
;;;        ("python-repoze.lru" ,python-repoze.lru)
;;;        ("python-webob" ,python-webob)))
;;;     (home-page "http://www.turbogears.org/")
;;;     (synopsis "Next generation TurboGears")
;;;     (description "Next generation TurboGears")
;;;     (license license:expat)))
;;; 
;;; (define-public python-backlash
;;;   (package
;;;     (name "python-backlash")
;;;     (version "0.3.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "backlash" version))
;;;        (sha256
;;;         (base32 "1832z1qqqn0iq1vx7yrm6hryjzjmhkg8mjbq0hcyn3g3kkay3xnx"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs `(("python-webob" ,python-webob)))
;;;     (native-inputs `(("python-setuptools-scm-git-archive" ,python-setuptools-scm-git-archive)))
;;;     (home-page "https://github.com/TurboGears/backlash")
;;;     (synopsis
;;;      "Standalone WebOb port of the Werkzeug Debugger with Python3 support meant to replace WebError in future TurboGears2")
;;;     (description
;;;      "Standalone WebOb port of the Werkzeug Debugger with Python3 support meant to replace WebError in future TurboGears2")
;;;     (license license:expat)))
;;; 
;;; (define-public python-tempita
;;;   (package
;;;     (name "python-tempita")
;;;     (version "0.5.2")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Tempita" version))
;;;        (sha256
;;;         (base32 "177wwq45slfyajd8csy477bmdmzipyw0dm7i85k3akb7m85wzkna"))))
;;;     (build-system python-build-system)
;;;     (native-inputs
;;;      `(("python-nose" ,python-nose)))
;;;     (home-page "http://pythonpaste.org/tempita/")
;;;     (synopsis "A very small text templating language")
;;;     (description "A very small text templating language")
;;;     (license license:expat)))
;;; 
;;; (define-public python-gearbox
;;;   (package
;;;     (name "python-gearbox")
;;;     (version "0.2.1")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "gearbox" version))
;;;        (sha256
;;;         (base32 "136jc9db0mnzx0yhixmwgqmyz43q8srjm8nza25v5wk3kpzbsd1l"))))
;;;     (build-system python-build-system)
;;;     (native-inputs
;;;      `(("python-nose" ,python-nose)))
;;;     (propagated-inputs
;;;      `(("python-hupper" ,python-hupper)
;;;        ("python-pastedeploy" ,python-pastedeploy)
;;;                                         ; ("python-tempita" ,python-tempita)
;;;        ))
;;;     (home-page "https://github.com/TurboGears/gearbox")
;;;     (synopsis
;;;      "Command line toolkit born as a PasteScript replacement for the TurboGears2 web framework")
;;;     (description
;;;      "Command line toolkit born as a PasteScript replacement for the TurboGears2 web framework")
;;;     (license license:expat)))
;;; 
;;; (define-public python-kallithea
;;;   (package
;;;     (name "python-kallithea")
;;;     (version "0.7.0")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (pypi-uri "Kallithea" version))
;;;        (sha256
;;;         (base32 "1ivnqp8p2a2g0gbfvlfbnwlbm9hql6j4kagwszg6d77fbbr9ix1x"))))
;;;     (build-system python-build-system)
;;;     (propagated-inputs
;;;      `(("python-alembic" ,python-alembic)
;;;        ("python-babel" ,python-babel)
;;;        ("python-backlash" ,python-backlash)
;;;        ("python-bcrypt" ,python-bcrypt)
;;;        ("python-beaker" ,python-beaker)
;;;        ("python-bleach" ,python-bleach)
;;;        ("python-celery" ,python-celery)
;;;        ("python-chardet" ,python-chardet)
;;;        ("python-click" ,python-click)
;;;        ("python-dateutil" ,python-dateutil)
;;;        ("python-decorator" ,python-decorator)
;;;        ("python-docutils" ,python-docutils)
;;;        ("python-dulwich" ,python-dulwich)
;;;        ("python-formencode" ,python-formencode)
;;;                                         ;("python-gearbox" ,python-gearbox)
;;;        ("python-ipaddr" ,python-ipaddr)
;;;        ("python-mako" ,python-mako)
;;;        ("python-markdown" ,python-markdown)
;;;        ("python-mercurial" ,mercurial)
;;;        ("python-paginate" ,python-paginate)
;;;                                         ; ("python-paginate-sqlalchemy" ,python-paginate-sqlalchemy)
;;;        ("python-paste" ,python-paste)
;;;        ("python-pip" ,python-pip)
;;;        ("python-pygments" ,python-pygments)
;;;        ("python-routes" ,python-routes)
;;;        ("python-sqlalchemy" ,python-sqlalchemy)
;;;        ("python-tgext.routes" ,python-tgext.routes)
;;;        ("python-turbogears2" ,python-turbogears2)
;;;        ("python-urlobject" ,python-urlobject)
;;;        ("python-waitress" ,python-waitress)
;;;        ("python-webhelpers2" ,python-webhelpers2)
;;;        ("python-webob" ,python-webob)
;;;        ("python-whoosh" ,python-whoosh)))
;;;     (arguments `(#:tests? #f))
;;;     (native-inputs
;;;      `(("python-pytest" ,python-pytest)))
;;;     (home-page "https://kallithea-scm.org/")
;;;     (synopsis
;;;      "Kallithea is a fast and powerful management tool for Mercurial and Git
;;;       with a built in push/pull server, full text search and code-review.")
;;;     (description
;;;      "Kallithea is a fast and powerful management tool for Mercurial and Git
;;;      with a built in push/pull server, full text search and code-review.")
;;;     (license #f)))
