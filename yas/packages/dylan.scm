4(define-module (yas packages dylan)
   #:use-module (gnu packages)
   #:use-module (gnu packages autotools)
   #:use-module (gnu packages base)
   #:use-module (gnu packages bison)
   #:use-module (gnu packages cmake)
   #:use-module (gnu packages compression)
   #:use-module (gnu packages curl)
   #:use-module (gnu packages elf)
   #:use-module (gnu packages flex)
   #:use-module (gnu packages gcc)
   #:use-module (gnu packages gdb)
   #:use-module (gnu packages bootstrap)
   #:use-module (gnu packages gettext)
   #:use-module (gnu packages jemalloc)
   #:use-module (gnu packages libffi)
   #:use-module (gnu packages linux)
   #:use-module (gnu packages llvm)
   #:use-module (gnu packages multiprecision)
   #:use-module (gnu packages ncurses)
   #:use-module (gnu packages perl)
   #:use-module (gnu packages pkg-config)
   #:use-module (gnu packages python)
   #:use-module (gnu packages ssh)
   #:use-module (gnu packages tls)
   #:use-module (gnu packages version-control)
   #:use-module (guix build-system gnu)
   #:use-module (guix build-system trivial)
   #:use-module ((guix build utils) #:select (alist-replace))
   #:use-module (guix download)
   #:use-module (guix git-download)
   #:use-module ((guix licenses) #:prefix license:)
   #:use-module (guix packages)
   #:use-module (guix utils)
   #:use-module (gnu packages bdw-gc)
   #:use-module (ice-9 match)
   #:use-module (srfi srfi-26))

                                        ; https://github.com/dylan-lang/opendylan/archive/refs/tags/v2011.1.zip

(define opendylan-2020.1.0-base
  (origin
    (method url-fetch)
    (uri "https://github.com/dylan-lang/opendylan/releases/download/v2020.1.0/opendylan-2020.1-x86_64-linux.tar.bz2")
    (sha256 (base32 "0glh86casndiz7vavar7smhbphg7qy2svpb3q9pc8qp3iw0nj4rd"))))

;;; /tmp/guix-build-opendylan-2020.1.0.drv-0/opendylan-2020.1.0/dylan-bin/opendylan-2020.1/bin/dylan-compiler

(define-public opendylan
  (let ((commit "2bd06ab50aec01932dacfd645c0770d5436e2a29")
        (revision "1"))
    (package
      (name "opendylan")
      (version "2020.1.0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url  "https://github.com/dylan-lang/opendylan")
               (commit commit)
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256 (base32 "0ygj3s1bjz00kl7ps0hdnswkfx83a42d8n9x8ais1vyfklr1rp8m"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("dylan-binary" ,opendylan-2020.1.0-base)
         ("autoconf" ,autoconf)
         ("automake" ,automake)
         ("git" ,git)
                                        ;  ("clang-11" ,clang-11)
                                        ;  ("clang-12" ,clang-12)
         ("libgc" ,libgc)
         ("clang-toolchain" ,clang-toolchain-11)
         ("clang-toolchain-12" ,clang-toolchain-12)
         ("gettext" ,gettext-minimal)
         ("libtool" ,libtool)
         ("perl" ,perl)
         ("pkg-config" ,pkg-config)))
      (arguments
       `(#:tests? #f
         #:phases
         (let ((dylan-bootstrap-path (string-append (getcwd) "/" ,name "-" ,version "/dylan-bin")))
           (modify-phases %standard-phases
             (add-after 'unpack 'unpack-bin
               (lambda* (#:key inputs #:allow-other-keys)
                 (mkdir-p dylan-bootstrap-path)
                 (with-directory-excursion dylan-bootstrap-path
                   (invoke "tar" "xvf" (assoc-ref inputs "dylan-binary")))))

             (add-before 'configure 'env-setting
               (lambda* (#:key outputs #:allow-other-keys)
                 (display "foo")
                 (setenv "PATH" (string-append (getenv "PATH")
                                               ":"
                                               dylan-bootstrap-path
                                               "/opendylan-2020.1/bin"))
                 #t))))))
      (inputs
       `(("zlib" ,zlib)))
      (home-page "http://opendylan.org")
      (synopsis "Open Dylan compiler and IDE")
      (description "Open Dylan compiler and IDE")
      (license license:gpl2+))))
