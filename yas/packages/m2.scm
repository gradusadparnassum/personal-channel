(define-module (yas packages m2)
  #:use-module ((guix licenses)
                #:select (gpl3+ gpl2+ lgpl2.1+ lgpl2.0+ fdl1.3+))
  #:use-module (gnu packages)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages dejagnu)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages docbook)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages perl)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 regex))

(define* (custom-gcc gcc name languages
                     #:optional
                     (search-paths (package-native-search-paths gcc))
                     #:key (separate-lib-output? #t))
  "Return a custom version of GCC that supports LANGUAGES.  Use SEARCH-PATHS
as the 'native-search-paths' field."
  (package (inherit gcc)
    (name name)
    (outputs (if separate-lib-output?
                 (package-outputs gcc)
                 (delete "lib" (package-outputs gcc))))
    (native-search-paths search-paths)
    (properties (alist-delete 'hidden? (package-properties gcc)))
    (arguments
     (substitute-keyword-arguments (package-arguments gcc)
       ((#:modules modules %gnu-build-system-modules)
        `(,@modules
          (srfi srfi-1)
          (srfi srfi-26)
          (ice-9 regex)))
       ((#:configure-flags flags)
        `(cons (string-append "--enable-languages="
                              ,(string-join languages ","))
               (remove (cut string-match "--enable-languages.*" <>)
                       ,flags)))
       ((#:phases phases)
        `(modify-phases ,phases
           (add-after 'install 'remove-broken-or-conflicting-files
             (lambda* (#:key outputs #:allow-other-keys)
               (for-each delete-file
                         (find-files (string-append (assoc-ref outputs "out") "/bin")
                                     ".*(c\\+\\+|cpp|g\\+\\+|gcov|gcc|gcc-.*)"))
               #t))))))))

(define %generic-search-paths
  ;; This is the language-neutral search path for GCC.  Entries in $CPATH are
  ;; not considered "system headers", which means GCC can raise warnings for
  ;; issues in those headers.  'CPATH' is the only one that works for
  ;; front-ends not in the C family.
  (list (search-path-specification
         (variable "CPATH")
         (files '("include")))
        (search-path-specification
         (variable "LIBRARY_PATH")
         (files '("lib" "lib64")))))

(define-public gccm2-next
  (package
    (inherit (custom-gcc gcc
                         "m2" '("m2")
                         %generic-search-paths))
    (version "c9afa63c3b00e843a703ae959000133a12fc4fcd")
    (synopsis "M2 frontend to GCC")
    (description
                 "This package is part of the GNU Compiler Collection and
provides the GNU compiler for the Modula 2 programming language.")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://gcc.gnu.org/git/gcc.git")
                    (commit version)))
	      (sha256
             (base32
              "13zs6vd2rapp42x154m479hg3h3lsafn3xhg06hp5hsldd9xr3nh"))
              (file-name (git-file-name "gm2" version))))))
