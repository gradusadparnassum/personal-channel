(define-module (yas packages headroom)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages haskell)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-crypto)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (guix build-system haskell)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix hg-download)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:select (alist-delete)))


(define-public ghc-vcs-ignore
  (package
    (name "ghc-vcs-ignore")
    (version "0.0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "vcs-ignore" version))
        (sha256
          (base32 "0cyab0mj225j79fpk4386iz4llpzisxpipb0v2i773psz4saw8ar"))))
    (build-system haskell-build-system)
    (inputs (list ghc-glob ghc-optparse-applicative))
    (native-inputs (list ghc-doctest ghc-hspec))
    (home-page "https://github.com/vaclavsvejcar/vcs-ignore")
    (synopsis "Library for handling files ignored by VCS systems.")
    (description
      "vcs-ignore is small Haskell library used to find, check and process files
ignored by selected VCS.")
    (license license:bsd-3)))

(define-public ghc-quickcheck-text
  (package
    (name "ghc-quickcheck-text")
    (version "0.1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "quickcheck-text" version))
        (sha256
          (base32 "02dbs0k6igmsa1hcw8yfvp09v7038vp4zlsp9706km3cmswgshj4"))))
    (build-system haskell-build-system)
    (inputs (list ghc-quickcheck))
    (home-page "https://github.com/olorin/quickcheck-text")
    (synopsis "Alternative arbitrary instance for Text")
    (description
      "The usual Arbitrary instance for Text (in
<https://hackage.haskell.org/package/quickcheck-instances quickcheck-instances>)
only has single-byte instances and so isn't an ideal representation of a valid
UTF-8 character.  This package has generators for one-, two- and three-byte
UTF-8 characters (all that are currently in use).")
    (license license:expat)))

(define-public ghc-string-interpolate
  (package
    (name "ghc-string-interpolate")
    (version "0.3.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "string-interpolate" version))
        (sha256
          (base32 "0hhzvrs9msyqsxwsqqm55lyxf85vhg4vcsszl735zsbs7431av69"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-split
            ghc-haskell-src-exts
            ghc-haskell-src-meta
            ghc-text-conversions
            ghc-utf8-string))
    (native-inputs
      (list ghc-quickcheck
            ghc-hspec
            ghc-hspec-core
            ghc-quickcheck-instances
            ghc-quickcheck-text
            ghc-quickcheck-unicode
            ghc-unordered-containers))
    (arguments
      `(#:cabal-revision
        ("2" "1blxy1ld69i8bg2340j0dcrcbdrqqnx5q8v47jda6183jfzwrxr6")))
    (home-page
      "https://gitlab.com/williamyaoh/string-interpolate/blob/master/README.md")
    (synopsis "Haskell string/text/bytestring interpolation that just works")
    (description
      "Unicode-aware string interpolation that handles all textual types. .  See the
README at
<https://gitlab.com/williamyaoh/string-interpolate/blob/master/README.md> for
more info.")
    (license license:bsd-3)))

(define-public ghc-req
  (package
    (name "ghc-req")
    (version "3.9.2")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "req" version))
        (sha256
          (base32 "17xkj5pypn4k6ncsahjc0h827kg3cyx5iy5q6iv1gvk8dwdiim0g"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-aeson
            ghc-authenticate-oauth
            ghc-blaze-builder
            ghc-case-insensitive
            ghc-connection
            ghc-http-api-data
            ghc-http-client
            ghc-http-client-tls
            ghc-http-types
            ghc-modern-uri
            ghc-monad-control
            ghc-retry
            ghc-transformers-base
            ghc-unliftio-core))
    (native-inputs
      (list ghc-quickcheck ghc-hspec ghc-hspec-core ghc-quickcheck ghc-hspec))
    (home-page "https://github.com/mrkkrp/req")
    (synopsis "HTTP client library")
    (description "HTTP client library.")
    (license license:bsd-3)))

(define-public ghc-pcre-heavy
  (package
    (name "ghc-pcre-heavy")
    (version "1.0.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "pcre-heavy" version))
        (sha256
          (base32 "1lfbjgvl55jh226n307c2w8mrb3l1myzbkjh4j0jfcb8nybzcp4a"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-base-compat
            ghc-pcre-light
            ghc-string-conversions
            ghc-semigroups))
    (native-inputs (list ghc-glob ghc-doctest))
    (arguments
      `(#:cabal-revision
        ("1" "14pprgwxkiaji3rqhsm0fv454wic6qxm7vy4a475yigadb1vz1ls")))
    (home-page "https://github.com/myfreeweb/pcre-heavy")
    (synopsis
      "A regexp (regex) library on top of pcre-light you can actually use.")
    (description
      "This package provides a regular expressions library that does not suck.  Based
on <https://hackage.haskell.org/package/pcre-light pcre-light>.  Takes and
returns <https://hackage.haskell.org/package/stringable Stringables> everywhere.
 Includes a QuasiQuoter for regexps that does compile time checking.  SEARCHES
FOR MULTIPLE MATCHES! DOES REPLACEMENT!")
    (license license:public-domain)))

(define-public ghc-lens-aeson
  (package
    (name "ghc-lens-aeson")
    (version "1.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "lens-aeson" version))
        (sha256
          (base32 "05jyn6rn0anhgfmk754gmmpcy5jv3ki213z4v243n9jvdjdlg7ms"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-lens
            ghc-vector
            ghc-unordered-containers
            ghc-attoparsec
            ghc-aeson
            ghc-scientific))
    (arguments
      `(#:cabal-revision
        ("1" "0l1hkkpp4blkhmbpswl6lqww6wpfm327f8knq1lskhnisbnfmj2j")))
    (home-page "http://github.com/lens/lens-aeson/")
    (synopsis "Law-abiding lenses for aeson")
    (description "Law-abiding lenses for aeson.")
    (license license:expat)))

(define-public ghc-rsa
  (package
    (name "ghc-rsa")
    (version "2.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "RSA" version))
        (sha256
          (base32 "0hchsqrxpfw7mqrqwscfy8ig1w2di6w3nxpzi873w0gibv2diibj"))))
    (build-system haskell-build-system)
    (inputs (list ghc-crypto-api ghc-crypto-pubkey-types ghc-sha))
    (native-inputs
      (list ghc-quickcheck
            ghc-tagged
            ghc-test-framework
            ghc-test-framework-quickcheck2))
    (home-page "http://hackage.haskell.org/package/RSA")
    (synopsis
      "Implementation of RSA, using the padding schemes of PKCS#1 v2.1.")
    (description
      "This library implements the RSA encryption and signature algorithms for
arbitrarily-sized ByteStrings.  While the implementations work, they are not
necessarily the fastest ones on the planet.  Particularly key generation.  The
algorithms included are based of RFC 3447, or the Public-Key Cryptography
Standard for RSA, version 2.1 (a.k.a, PKCS#1 v2.1).")
    (license license:bsd-3)))

(define-public ghc-crypto-pubkey-types
  (package
    (name "ghc-crypto-pubkey-types")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "crypto-pubkey-types" version))
        (sha256
          (base32 "0q0wlzjmpx536h1zcdzrpxjkvqw8abj8z0ci38138kpch4igbnby"))))
    (build-system haskell-build-system)
    (inputs (list ghc-asn1-types ghc-asn1-encoding))
    (home-page "http://github.com/vincenthz/hs-crypto-pubkey-types")
    (synopsis "Generic cryptography Public keys algorithm types")
    (description "Generic cryptography public keys algorithm types")
    (license license:bsd-3)))

(define-public ghc-authenticate-oauth
  (package
    (name "ghc-authenticate-oauth")
    (version "1.7")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "authenticate-oauth" version))
        (sha256
          (base32 "0y4v46rn0cvm0sr1v8qq1zgzllrlrr3ji5gij1xprgf1zsazcvvl"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-http-client
            ghc-crypto-pubkey-types
            ghc-rsa
            ghc-data-default
            ghc-base64-bytestring
            ghc-sha
            ghc-random
            ghc-http-types
            ghc-blaze-builder
            ghc-transformers-compat))
    (home-page "http://github.com/yesodweb/authenticate")
    (synopsis
      "Library to authenticate with OAuth for Haskell web applications.")
    (description
      "API docs and the README are available at
<http://www.stackage.org/package/authenticate-oauth>.")
    (license license:bsd-3)))

(define-public ghc-wreq
  (package
    (name "ghc-wreq")
    (version "0.5.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "wreq" version))
        (sha256
          (base32 "0zv51048p0r7vhamml3ps9nr11yi9fxz2w31qcz053bw3z9ivwxw"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-psqueues
            ghc-aeson
            ghc-attoparsec
            ghc-authenticate-oauth
            ghc-base16-bytestring
            ghc-case-insensitive
            ghc-cryptonite
            ghc-hashable
            ghc-http-client
            ghc-http-client-tls
            ghc-http-types
            ghc-lens
            ghc-lens-aeson
            ghc-memory
            ghc-mime-types
            ghc-time-locale-compat
            ghc-unordered-containers))
    (native-inputs
      (list ghc-hunit
            ghc-quickcheck
            ghc-aeson-pretty
            ghc-base64-bytestring
            ghc-network-info
            ghc-snap-core
            ghc-snap-server
            ghc-temporary
            ghc-test-framework
            ghc-test-framework-hunit
            ghc-test-framework-quickcheck2
            ghc-unix-compat
            ghc-uuid
            ghc-vector
            ghc-doctest
            ghc-cabal-doctest))
    (home-page "http://www.serpentine.com/wreq")
    (synopsis "An easy-to-use HTTP client library.")
    (description
      ".  A web client library that is designed for ease of use. .  Tutorial:
<http://www.serpentine.com/wreq/tutorial.html> .  Features include: . * Simple
but powerful `lens`-based API . * A solid test suite, and built on reliable
libraries like http-client and lens . * Session handling includes connection
keep-alive and pooling, and cookie persistence . * Automatic response body
decompression . * Powerful multipart form and file upload handling . * Support
for JSON requests and responses, including navigation of schema-less responses .
* Basic and OAuth2 bearer authentication . * Early TLS support via the tls
package")
    (license license:bsd-3)))

(define-public ghc-mustache
  (package
    (name "ghc-mustache")
    (version "2.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "mustache" version))
        (sha256
          (base32 "0pnvnqrm7sd1iglh298yl91mv69p3ra25s5xrlk73kb56albdbaq"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-aeson
            ghc-scientific
            ghc-th-lift
            ghc-unordered-containers
            ghc-vector
            ghc-cmdargs
            ghc-yaml))
    (native-inputs
      (list ghc-base-unicode-symbols
            ghc-hspec
            ghc-lens
            ghc-tar
            ghc-wreq
            ghc-zlib
            ghc-hspec
            ghc-temporary))
    (arguments
      `(#:cabal-revision
        ("1" "11y1mdb9p5b86ld8giy5n5idylnmyafp170rhp45vmdjhyfjivjv")))
    (home-page "https://github.com/JustusAdam/mustache")
    (synopsis "A mustache template parser library.")
    (description
      "Allows parsing and rendering template files with mustache markup.  See the
mustache <http://mustache.github.io/mustache.5.html language reference>. .
Implements the mustache spec version 1.1.3. . /Note/: Versions including and
beyond 0.4 are compatible with ghc 7.8 again.")
    (license license:bsd-3)))

(define-public ghc-modern-uri
  (package
    (name "ghc-modern-uri")
    (version "0.3.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "modern-uri" version))
        (sha256
          (base32 "018hiiqx6n272mwbmhd5j9wlzyz0x7ppa9jsrv4zx1nb6n7shkh5"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-quickcheck
            ghc-contravariant
            ghc-megaparsec
            ghc-profunctors
            ghc-reflection
            ghc-tagged))
    (native-inputs (list ghc-hspec ghc-hspec-megaparsec))
    (arguments
      `(#:cabal-revision
        ("1" "01v497wkcida80xlkg25yhppb711ynyx2zyv9hdyzrflq8pz4g6w")))
    (home-page "https://github.com/mrkkrp/modern-uri")
    (synopsis "Modern library for working with URIs")
    (description "Modern library for working with URIs.")
    (license license:bsd-3)))

(define-public ghc-linear-base
  (package
    (name "ghc-linear-base")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "linear-base" version))
        (sha256
          (base32 "00n7rmvkjg42n1pba1y1lziw9a8gyhix15rw13qsyymi8bdr8k82"))))
    (build-system haskell-build-system)
    (inputs (list ghc-hashable ghc-storable-tuple ghc-vector ghc-primitive))
    (native-inputs
      (list ghc-hedgehog
            ghc-tasty
            ghc-tasty-hedgehog
            ghc-mmorph
            ghc-tasty
            ghc-tasty-hedgehog
            ghc-hedgehog))
    (home-page "https://github.com/tweag/linear-base#README")
    (synopsis "Standard library for linear types.")
    (description "Please see README.md.")
    (license license:expat)))

(define-public ghc-one-liner
  (package
    (name "ghc-one-liner")
    (version "2.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "one-liner" version))
        (sha256
          (base32 "0al9wavxx23xbalqw0cdlhq01kx8kyhg33fipwmn5617z3ddir6v"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-contravariant
            ghc-bifunctors
            ghc-profunctors
            ghc-tagged
            ghc-linear-base))
    (native-inputs (list ghc-hunit))
    (home-page "https://github.com/sjoerdvisscher/one-liner")
    (synopsis "Constraint-based generics")
    (description
      "Write short and concise generic instances of type classes.  one-liner is
particularly useful for writing default implementations of type class methods.")
    (license license:bsd-3)))

(define-public ghc-indexed-profunctors
  (package
    (name "ghc-indexed-profunctors")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "indexed-profunctors" version))
        (sha256
          (base32 "1cbccbvrx73drr1jf3yyw0rp1mcfv3jc1rvdcby5xxx4ja543fjs"))))
    (build-system haskell-build-system)
    (home-page "http://hackage.haskell.org/package/indexed-profunctors")
    (synopsis "Utilities for indexed profunctors")
    (description
      "This package contains basic definitions related to indexed profunctors.  These
are primarily intended as internal utilities to support the @optics@ and
@generic-lens@ package families.")
    (license license:bsd-3)))

(define-public ghc-generic-lens-core
  (package
    (name "ghc-generic-lens-core")
    (version "2.2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "generic-lens-core" version))
        (sha256
          (base32 "0nj6ldwfidbyid85076gy8f5sa02xnbdhn51pkyg4dfqqz4r4hg8"))))
    (build-system haskell-build-system)
    (inputs (list ghc-indexed-profunctors))
    (home-page "https://github.com/kcsongor/generic-lens")
    (synopsis "Generically derive traversals, lenses and prisms.")
    (description
      "This library uses GHC.Generics to derive efficient optics (traversals, lenses
and prisms) for algebraic data types in a type-directed way, with a focus on
good type inference and error messages when possible. .  This package is the
shared internal logic of the @<https://hackage.haskell.org/package/generic-lens
generic-lens>@ and @<https://hackage.haskell.org/package/generic-optics
generic-optics>@ libraries.")
    (license license:bsd-3)))

(define-public ghc-generic-lens
  (package
    (name "ghc-generic-lens")
    (version "2.2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "generic-lens" version))
        (sha256
          (base32 "0n61hrfciryl33w6vxd5p1yqjfxl02n717dahlvb6fxlg9339nxp"))))
    (build-system haskell-build-system)
    (inputs (list ghc-generic-lens-core ghc-profunctors))
    (native-inputs
      (list ghc-lens
            ghc-inspection-testing
            ghc-hunit
            ghc-lens
            ghc-hunit
            ghc-lens
            ghc-hunit
            ghc-doctest))
    (home-page "https://github.com/kcsongor/generic-lens")
    (synopsis "Generically derive traversals, lenses and prisms.")
    (description
      "This library uses GHC.Generics to derive efficient optics (traversals, lenses
and prisms) for algebraic data types in a type-directed way, with a focus on
good type inference and error messages when possible. .  The library exposes a
van Laarhoven interface.  For an alternative interface, supporting an opaque
optic type, see @<https://hackage.haskell.org/package/generic-optics
generic-optics>@.")
    (license license:bsd-3)))

(define-public ghc-show-combinators
  (package
    (name "ghc-show-combinators")
    (version "0.2.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "show-combinators" version))
        (sha256
          (base32 "07ds87ldl9165hj3k5h84iawc6vqlbggni3dg1nhbxww1spxn0n9"))))
    (build-system haskell-build-system)
    (arguments
      `(#:cabal-revision
        ("2" "0n3xlpm41wpw1ybmacg9s7150nx00qrdlw2rq4fzz7iw7333cyjx")))
    (home-page "https://github.com/Lysxia/show-combinators#readme")
    (synopsis "Combinators to write Show instances")
    (description
      "This package provides a minimal pretty-printing library for Show instances in
Haskell.")
    (license license:expat)))

(define-public ghc-ap-normalize
  (package
    (name "ghc-ap-normalize")
    (version "0.1.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "ap-normalize" version))
        (sha256
          (base32 "1212zxc4qn6msk0w13yhrza2qjs79h78misllb4chng75jqi61l2"))))
    (build-system haskell-build-system)
    (native-inputs (list ghc-inspection-testing ghc-inspection-testing))
    (home-page "http://hackage.haskell.org/package/ap-normalize")
    (synopsis "Self-normalizing applicative expressions")
    (description
      "An applicative functor transformer to normalize expressions using @(\\<$>)@,
@(\\<*>)@, and @pure@ into a linear list of actions.  See \"ApNormalize\" to get
started.")
    (license license:expat)))

(define-public ghc-generic-data
  (package
    (name "ghc-generic-data")
    (version "0.9.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "generic-data" version))
        (sha256
          (base32 "0hs5ahl1nx61kw5j0pnwgjrph7jgqq0djma956ksz6aivzldjf7q"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-ap-normalize
            ghc-base-orphans
            ghc-contravariant
            ghc-show-combinators))
    (native-inputs
      (list ghc-tasty
            ghc-tasty-hunit
            ghc-tasty
            ghc-tasty-hunit
            ghc-tasty
            ghc-tasty-hunit
            ghc-generic-lens
            ghc-tasty
            ghc-tasty-hunit
            ghc-generic-lens
            ghc-one-liner
            ghc-inspection-testing
            ghc-unordered-containers))
    (home-page "https://github.com/Lysxia/generic-data#readme")
    (synopsis "Deriving instances with GHC.Generics and related utilities")
    (description
      "Generic implementations of standard type classes.  Operations on generic
representations to help using \"GHC.Generics\".  See README.")
    (license license:expat)))

(define-public ghc-headroom
  (package
    (name "ghc-headroom")
    (version "0.4.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (hackage-uri "headroom" version))
        (sha256
          (base32 "1rg1n3pa6lh1a1flk8g8r5m1s77hl0cyd0c129rw8h1w2w2kkpj0"))))
    (build-system haskell-build-system)
    (inputs
      (list ghc-aeson
            ghc-either
            ghc-extra
            ghc-file-embed
            ghc-generic-data
            ghc-http-client
            ghc-http-types
            ghc-microlens
            ghc-microlens-th
            ghc-modern-uri
            ghc-mustache
            ghc-optparse-applicative
            ghc-pcre-heavy
            ghc-pcre-light
            ghc-req
            ghc-rio
            ghc-string-interpolate
            ghc-vcs-ignore
            ghc-yaml))
    (native-inputs (list ghc-doctest ghc-quickcheck ghc-hspec))
    (home-page "https://github.com/vaclavsvejcar/headroom")
    (synopsis "License Header Manager")
    (description
      "Would you like to have nice, up-to-date license/copyright headers in your source
code files but hate to manage them by hand? Then Headroom is the right tool for
you! Now you can define your license header as Mustache template, put all the
variables (such as author's name, year, etc.) into the YAML config file and
Headroom will take care to add such license headers to all your source code
files.")
    (license license:bsd-3)))

