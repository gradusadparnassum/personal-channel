(define-module (yas packages emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (guix build-system emacs)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix hg-download)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:select (alist-delete)))

(define-public emacs-vertico-alt
  (package
    (name "emacs-vertico-alt")
    (version "0.14")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://elpa.gnu.org/packages/vertico-" version ".tar"))
       (sha256
        (base32 "1lvfvrmfi6f1jcf356rj1zl2bcbqxas7wi3yb93mxpn37l22l8mi"))))
    (build-system emacs-build-system)
    (home-page "https://melpa.org/#/vertico")
    (synopsis "VERTical Interactive COmpletion")
    (description "A performant and minimalistic vertical completion UI based on the default completion system")
    (license license:expat)))

(define-public emacs-affe
  (package
    (name "emacs-affe")
    (version "20211011.725")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://melpa.org/packages/affe-" version ".tar"))
       (sha256
        (base32 "1apgf1zmi4dmwk1b82yxs75191f3w29ypgkp2xz2238dh75l2gys"))))
    (build-system emacs-build-system)
    (propagated-inputs `(("emacs-consult" ,emacs-consult)))
    (home-page "https://github.com/minad/affe")
    (synopsis "Asynchronous Fuzzy Finder for Emacs")
    (description "Asynchronous Fuzzy Finder for Emacs")
    (license #f)))
;;;
;;;
(define-public emacs-consult
  (package
    (name "emacs-consult")
    (version "20211018.1957")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://melpa.org/packages/consult-"
                           version ".tar"))
       (sha256
        (base32 "0h939lfwld3nmjic12m5a4yg522xjgfiy9q1mcinmp984ii2a8iv"))))
    (build-system emacs-build-system)
    (home-page "https://melpa.org/#/consult")
    (synopsis "Consulting completing-read")
    (description "Consult provides practical commands based on the Emacs completion function completing-read.")
    (license license:expat)))
;;;
;;; (define-public emacs-marginalia
;;;   (package
;;;     (name "emacs-marginalia")
;;;     (version "20210905.1700" )
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (string-append "https://melpa.org/packages/marginalia-"
;;;                            version ".el"))
;;;        (sha256
;;;         (base32
;;;          "0ddx07lbf17w9kq6wmn2pdmf0rnv8w8wf0w3f7lgam6iwk6z79gi"))))
;;;     (build-system emacs-build-system)
;;;     (home-page "https://melpa.org/#/marginalia")
;;;     (synopsis "")
;;;     (description "")
;;;     (license license:gpl3+)))
;;;
;;; (define-public emacs-dylan
;;;   (package
;;;     (name "emacs-dylan")
;;;     (version "20210613.1431")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (string-append "https://melpa.org/packages/dylan-"
;;;                            version ".tar"))
;;;        (sha256
;;;         (base32
;;;          "1pyg8vs1d2cgmkq52c1pgj4sv7flsr42h9y3qdaw1zvdhiipi8yc"))))
;;;     (build-system emacs-build-system)
;;;     (home-page "https://github.com/dylan-lang/dylan-emacs-support")
;;;     (synopsis "Emacs mode for indenting and highlighting Dylan code")
;;;     (description
;;;      "The dylan-mode major mode to edit Dylan code.")
;;;     (license license:gpl3+)))
;;;
;;; (define-public emacs-corfu
;;;   (package
;;;     (name "emacs-corfu")
;;;     (version "0.13")
;;;     (source
;;;      (origin
;;;        (method url-fetch)
;;;        (uri (string-append
;;;              "https://elpa.gnu.org/packages/corfu-"
;;;              version
;;;              ".tar"))
;;;        (sha256
;;;         (base32 "1sf7ll25ry7vwff4bvqgdh84zviqj6wifmqdb2z8hf12awz63icz"))))
;;;     (build-system emacs-build-system)
;;;     (home-page "https://github.com/minad/corfu")
;;;     (synopsis "Completion Overlay Region FUnction")
;;;     (description
;;;      "#+title: corfu.el - Completion Overlay Region FUnction")
;;;     (license license:gpl3+)))
;;;
;;;                                         ;(define-public emacs-dime
;;;                                         ;(package
;;;                                         ;(name "emacs-dime")
;;;                                         ;(version "20210329.604")
;;;                                         ;(source
;;;                                         ;(origin
;;;                                         ;(method url-fetch)
;;;                                         ;(uri (string-append "https://melpa.org/packages/dime-"
;;;                                         ;version ".tar"))
;;;                                         ;(sha256
;;;                                         ;(base32
;;;                                         ;"085yfq0p75a421mlrw24n3fdafv40i4hfk1ckakdh0916k0yjxdi"))))
;;;                                         ;(build-system emacs-build-system)
;;;                                         ;(home-page "https://github.com/dylan-lang/dylan-emacs-support")
;;;                                         ;(synopsis "Emacs mode for indenting and highlighting Dylan code")
;;;                                         ;(description
;;;                                         ;"The dylan-mode major mode to edit Dylan code.")
;;;                                         ;(license license:gpl2+)))
