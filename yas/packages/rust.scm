(define-module (yas packages rust)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (guix download)
  #:use-module (guix build-system cargo)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (ice-9 match))

(define unknown-license! "unknown")

(define-public rust-select-0.5
  (package
    (name "rust-select")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "select" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dvnb12fqczq0mqgyh7pafkhngv8478x0y3sxy5ngj7w1bwn3q4f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-html5ever" ,rust-html5ever-0.25)
         ("rust-speculate" ,rust-speculate-0.1)
         ("rust-markup5ever-rcdom" ,rust-markup5ever-rcdom-0.1))))
    (home-page "https://github.com/utkarshkukreti/select.rs")
    (synopsis
      "A library to extract useful data from HTML documents, suitable for web scraping.")
    (description
      "This package provides a library to extract useful data from HTML documents,
suitable for web scraping.")
    (license license:expat)))

(define-public rust-topological-sort-0.1
  (package
    (name "rust-topological-sort")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "topological-sort" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g478pajmv620cl2hpppacdlrhdrmqrmcvvq76abkcd4vr17yz5a"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/gifnksm/topological-sort-rs")
    (synopsis "Performs topological sorting.")
    (description "Performs topological sorting.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pulldown-cmark-0.9
  (package
    (name "rust-pulldown-cmark")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pulldown-cmark" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ml1l6adgsmpxipjcm3hz5xmrfybr5z9ldbcwhxapjdh8jjrgw9l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-getopts" ,rust-getopts-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-unicase" ,rust-unicase-2))))
    (home-page "https://github.com/raphlinus/pulldown-cmark")
    (synopsis "A pull parser for CommonMark")
    (description "This package provides a pull parser for CommonMark")
    (license license:expat)))

(define-public rust-opener-0.5
  (package
    (name "rust-opener")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opener" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lkrn4fv1h4m8gmp7ll6x7vjvb6kls2ngwa5cgsh2ix5fb6yp8sf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bstr" ,rust-bstr-0.2) ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/Seeker14491/opener")
    (synopsis "Open a file or link using the system default program.")
    (description "Open a file or link using the system default program.")
    (license (list license:expat license:asl2.0))))

(define-public rust-warp-0.3
  (package
    (name "rust-warp")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "warp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zjqbg2j1fdpqq74bi80hmvyakf1f771d7vrmkqvg90lj4g4xvrw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-log" ,rust-log-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-multipart" ,rust-multipart-0.18)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.15)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/seanmonstar/warp")
    (synopsis "serve the web at warp speeds")
    (description "serve the web at warp speeds")
    (license license:expat)))

(define-public rust-globset-0.4
  (package
    (name "rust-globset")
    (version "0.4.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "globset" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gdzphnjjc0wdaawsq3n1nnypv9ja4prhca2n66hcahay2gksihh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-bstr" ,rust-bstr-0.2)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page
      "https://github.com/BurntSushi/ripgrep/tree/master/crates/globset")
    (synopsis
      "Cross platform single glob and glob set matching. Glob set matching is the
process of matching one or more glob patterns against a single candidate path
simultaneously, and returning all of the globs that matched.
")
    (description
      "Cross platform single glob and glob set matching.  Glob set matching is the
process of matching one or more glob patterns against a single candidate path
simultaneously, and returning all of the globs that matched.")
    (license (list license:unlicense license:expat))))

(define-public rust-rust-embed-utils-7
  (package
    (name "rust-rust-embed-utils")
    (version "7.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed-utils" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v0sihipyiw7wfasa4xdvx5brgwmx5sza7djx96i0aa8dqicf8md"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-globset" ,rust-globset-0.4)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis "Utilities for rust-embed")
    (description "Utilities for rust-embed")
    (license license:expat)))

(define-public rust-rust-embed-impl-6
  (package
    (name "rust-rust-embed-impl")
    (version "6.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c6xgqdckydg3nx8c6zxz8cs157pczwq7s3bpir0rgx29gi67rwl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rust-embed-utils" ,rust-rust-embed-utils-7)
         ("rust-shellexpand" ,rust-shellexpand-2)
         ("rust-syn" ,rust-syn-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis
      "Rust Custom Derive Macro which loads files into the rust binary at compile time during release and loads the file from the fs during dev")
    (description
      "Rust Custom Derive Macro which loads files into the rust binary at compile time
during release and loads the file from the fs during dev")
    (license license:expat)))

(define-public rust-tokio-util-0.6
  (package
    (name "rust-tokio-util")
    (version "0.6.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h2cc3ickn6wj5c0bhw8v5drzrwr5r6n0rjbxgc6qdsx7scf36cy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://tokio.rs")
    (synopsis "Additional utilities for working with Tokio.
")
    (description "Additional utilities for working with Tokio.")
    (license license:expat)))

(define-public rust-thiserror-impl-1
  (package
    (name "rust-thiserror-impl")
    (version "1.0.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0jviwmvx6wzawsj6c9msic7h419wmsbjagl9dzhpydkzc8zzscma"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/dtolnay/thiserror")
    (synopsis "Implementation detail of the `thiserror` crate")
    (description "Implementation detail of the `thiserror` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-thiserror-1
  (package
    (name "rust-thiserror")
    (version "1.0.30")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thiserror" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05y4wm29ck8flwq5k1q6nhwh00a3b30cz3xr0qvnbwad5vjsnjw5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror-impl" ,rust-thiserror-impl-1))))
    (home-page "https://github.com/dtolnay/thiserror")
    (synopsis "derive(Error)")
    (description "derive(Error)")
    (license (list license:expat license:asl2.0))))

(define-public rust-sse-codec-0.3
  (package
    (name "rust-sse-codec")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sse-codec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nh8b1y2k5lsvcva15da4by935bavirfpavs0d54pi2h2f0rz9c4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-bytes" ,rust-bytes-0.5)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-codec" ,rust-futures-codec-0.4)
         ("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/goto-bus-stop/sse-codec")
    (synopsis "async Server-Sent Events protocol encoder/decoder")
    (description "async Server-Sent Events protocol encoder/decoder")
    (license license:mpl2.0)))

(define-public rust-crc16-0.4
  (package
    (name "rust-crc16")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crc16" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zzwb5iv51wnh96532cxkk4aa8ys47rhzrjy98wqcys25ks8k01k"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/blackbeam/rust-crc16")
    (synopsis "A CRC16 implementation")
    (description "This package provides a CRC16 implementation")
    (license license:expat)))

(define-public rust-combine-4
  (package
    (name "rust-combine")
    (version "4.6.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "combine" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qihymj493vvs054gzpcmp4lzb098zrj2p9miv19yzvrrjm2gdsh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-0.5)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-regex" ,rust-regex-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio" ,rust-tokio-0.3)
         ("rust-tokio" ,rust-tokio-0.2)
         ("rust-tokio-util" ,rust-tokio-util-0.6))))
    (home-page "https://github.com/Marwes/combine")
    (synopsis
      "Fast parser combinators on arbitrary streams with zero-copy support.")
    (description
      "Fast parser combinators on arbitrary streams with zero-copy support.")
    (license license:expat)))

(define-public rust-redis-0.21
  (package
    (name "rust-redis")
    (version "0.21.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redis" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lh88wg4q4d3v3fc5r7hx25giaygm506xqd0aq404nkziprvb00s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arc-swap" ,rust-arc-swap-1)
         ("rust-async-native-tls" ,rust-async-native-tls-0.3)
         ("rust-async-std" ,rust-async-std-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-combine" ,rust-combine-4)
         ("rust-crc16" ,rust-crc16-0.4)
         ("rust-dtoa" ,rust-dtoa-0.4)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-itoa" ,rust-itoa-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-r2d2" ,rust-r2d2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/mitsuhiko/redis-rs")
    (synopsis "Redis driver for Rust.")
    (description "Redis driver for Rust.")
    (license license:bsd-3)))

(define-public rust-priority-queue-1
  (package
    (name "rust-priority-queue")
    (version "1.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "priority-queue" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w6a4wkxm7h7qhxqgivgxbixw51czmkd83x1vr0gqg4dq054ifh0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-autocfg" ,rust-autocfg-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/garro95/priority-queue")
    (synopsis
      "A Priority Queue implemented as a heap with a function to efficiently change the priority of an item.")
    (description
      "This package provides a Priority Queue implemented as a heap with a function to
efficiently change the priority of an item.")
    (license (list license:lgpl3 license:mpl2.0))))

(define-public rust-proc-macro-crate-1
  (package
    (name "rust-proc-macro-crate")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro-crate" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10vgiwpp9rbi999pbn67p3r560z92bpfqszpsfs8ky6ai5lcxfhy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-thiserror" ,rust-thiserror-1) ("rust-toml" ,rust-toml-0.5))))
    (home-page "https://github.com/bkchr/proc-macro-crate")
    (synopsis "Replacement for crate (macro_rules keyword) in proc-macros
")
    (description "Replacement for crate (macro_rules keyword) in proc-macros")
    (license (list license:asl2.0 license:expat))))

(define-public rust-poem-derive-1
  (package
    (name "rust-poem-derive")
    (version "1.2.43")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "poem-derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g446bp3858bzvgnb247lgdlsk7mxyvlz4lbpk3z91g3h4sqskkr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/poem-web/poem")
    (synopsis "Macros for poem")
    (description "Macros for poem")
    (license (list license:expat license:asl2.0))))

(define-public rust-thread-id-4
  (package
    (name "rust-thread-id")
    (version "4.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "thread-id" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zvikdngp0950hi0jgiipr8l36rskk1wk7pc8cd43xr3g5if1psz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/ruuda/thread-id")
    (synopsis "Get a unique thread ID")
    (description "Get a unique thread ID")
    (license (list license:expat license:asl2.0))))

(define-public rust-redox-syscall-0.2
  (package
    (name "rust-redox-syscall")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "redox_syscall" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zq36bhw4c6xig340ja1jmr36iy0d3djp8smsabxx71676bg70w3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1))))
    (home-page "https://gitlab.redox-os.org/redox-os/syscall")
    (synopsis "A Rust library to access raw Redox system calls")
    (description
      "This package provides a Rust library to access raw Redox system calls")
    (license license:expat)))

(define-public rust-object-0.27
  (package
    (name "rust-object")
    (version "0.27.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "object" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ygv9zgi9wz6q5f2z9xn72i0c97jjr1dgj30kbyicdhxk8zivb37"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-crc32fast" ,rust-crc32fast-1)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-wasmparser" ,rust-wasmparser-0.57))))
    (home-page "https://github.com/gimli-rs/object")
    (synopsis
      "A unified interface for reading and writing object file formats.")
    (description
      "This package provides a unified interface for reading and writing object file
formats.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-gimli-0.26
  (package
    (name "rust-gimli")
    (version "0.26.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gimli" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m0vi36ypv4gx9gzcw6y456yqnlypizhwlcqrmg6vkwd0lnkgk3q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-fallible-iterator" ,rust-fallible-iterator-0.2)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-stable-deref-trait" ,rust-stable-deref-trait-1))))
    (home-page "https://github.com/gimli-rs/gimli")
    (synopsis "A library for reading and writing the DWARF debugging format.")
    (description
      "This package provides a library for reading and writing the DWARF debugging
format.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-addr2line-0.17
  (package
    (name "rust-addr2line")
    (version "0.17.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "addr2line" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0sw16zqy6w0ar633z69m7lw6gb0k1y7xj3387a8wly43ij5div5r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-cpp-demangle" ,rust-cpp-demangle-0.3)
         ("rust-fallible-iterator" ,rust-fallible-iterator-0.2)
         ("rust-gimli" ,rust-gimli-0.26)
         ("rust-object" ,rust-object-0.27)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-smallvec" ,rust-smallvec-1))))
    (home-page "https://github.com/gimli-rs/addr2line")
    (synopsis
      "A cross-platform symbolication library written in Rust, using `gimli`")
    (description
      "This package provides a cross-platform symbolication library written in Rust,
using `gimli`")
    (license (list license:asl2.0 license:expat))))

(define-public rust-backtrace-0.3
  (package
    (name "rust-backtrace")
    (version "0.3.63")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "backtrace" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dp1dik2riphgslibafp3kzj95f8kgx42wkh4qghc4v5pbc2j5ij"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-addr2line" ,rust-addr2line-0.17)
         ("rust-cc" ,rust-cc-1)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cpp-demangle" ,rust-cpp-demangle-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-miniz-oxide" ,rust-miniz-oxide-0.4)
         ("rust-object" ,rust-object-0.27)
         ("rust-rustc-demangle" ,rust-rustc-demangle-0.1)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/rust-lang/backtrace-rs")
    (synopsis
      "A library to acquire a stack trace (backtrace) at runtime in a Rust program.
")
    (description
      "This package provides a library to acquire a stack trace (backtrace) at runtime
in a Rust program.")
    (license (list license:expat license:asl2.0))))

(define-public rust-parking-lot-core-0.8
  (package
    (name "rust-parking-lot-core")
    (version "0.8.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05ij4zxsylx99srbq8qd1k2wiwaq8krkf9y4cqkhvb5wjca8wvnp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-petgraph" ,rust-petgraph-0.5)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-thread-id" ,rust-thread-id-4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "An advanced API for creating custom synchronization primitives.")
    (description
      "An advanced API for creating custom synchronization primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-lock-api-0.4
  (package
    (name "rust-lock-api")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lock_api" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "028izfyraynijd9h9x5miv1vmg6sjnw1v95wgm7f4xlr7h4lsaki"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-owning-ref" ,rust-owning-ref-0.4)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "Wrappers to create fully-featured Mutex and RwLock types. Compatible with no_std.")
    (description
      "Wrappers to create fully-featured Mutex and RwLock types.  Compatible with
no_std.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-parking-lot-0.11
  (package
    (name "rust-parking-lot")
    (version "0.11.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "parking_lot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16gzf41bxmm10x82bla8d6wfppy9ym3fxsmdjyvn61m66s0bf5vx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-instant" ,rust-instant-0.1)
         ("rust-lock-api" ,rust-lock-api-0.4)
         ("rust-parking-lot-core" ,rust-parking-lot-core-0.8))))
    (home-page "https://github.com/Amanieu/parking_lot")
    (synopsis
      "More compact and efficient implementations of the standard synchronization primitives.")
    (description
      "More compact and efficient implementations of the standard synchronization
primitives.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-opentelemetry-semantic-conventions-0.8
  (package
    (name "rust-opentelemetry-semantic-conventions")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-semantic-conventions" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1sbh48ghkmjz06ylbirvhvr9g7xzax83ix31p4khz2wy6ciwispz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opentelemetry" ,rust-opentelemetry-0.16))))
    (home-page
      "https://github.com/open-telemetry/opentelemetry-rust/tree/main/opentelemetry-semantic-conventions")
    (synopsis "Semantic conventions for OpenTelemetry")
    (description "Semantic conventions for OpenTelemetry")
    (license license:asl2.0)))

(define-public rust-protobuf-codegen-2
  (package
    (name "rust-protobuf-codegen")
    (version "2.25.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lxq7k34hgf7pfqaar826qmdilf1d2yh1bnvq99nckdx126cky1x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis
      "Code generator for rust-protobuf.

Includes a library and `protoc-gen-rust` binary.

See `protoc-rust` and `protobuf-codegen-pure` crates.
")
    (description
      "Code generator for rust-protobuf.

Includes a library and `protoc-gen-rust` binary.

See `protoc-rust` and `protobuf-codegen-pure` crates.")
    (license license:expat)))

(define-public rust-protobuf-codegen-pure-2
  (package
    (name "rust-protobuf-codegen-pure")
    (version "2.25.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf-codegen-pure" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xx85s56zly9gr4p009yfrx76x0i1wn08c4dvxj56h0rm3i76jir"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen" ,rust-protobuf-codegen-2))))
    (home-page
      "https://github.com/stepancheg/rust-protobuf/tree/master/protobuf-codegen-pure/")
    (synopsis
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP
")
    (description
      "Pure-rust codegen for protobuf using protobuf-parser crate

WIP")
    (license license:expat)))

(define-public rust-protobuf-2
  (package
    (name "rust-protobuf")
    (version "2.25.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "protobuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m2pb79b73pkmcsmw9s0x6s4n1z7qbdprycx2mc226k2j7hjghs7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/stepancheg/rust-protobuf/")
    (synopsis "Rust implementation of Google protocol buffers
")
    (description "Rust implementation of Google protocol buffers")
    (license license:expat)))

(define-public rust-procfs-0.9
  (package
    (name "rust-procfs")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "procfs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kfmlpx902czawrn54zbs0918k0bxi22lv931zds4l44q7h0k25b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/eminence/procfs")
    (synopsis "Interface to the linux procfs pseudo-filesystem")
    (description "Interface to the linux procfs pseudo-filesystem")
    (license (list license:expat license:asl2.0))))

(define-public rust-prometheus-0.12
  (package
    (name "rust-prometheus")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "prometheus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "175r0lz6qsqjzm8mpcnlzwqng6ybm7dir2qgyp99401qca6sm1jr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-procfs" ,rust-procfs-0.9)
         ("rust-protobuf" ,rust-protobuf-2)
         ("rust-protobuf-codegen-pure" ,rust-protobuf-codegen-pure-2)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/tikv/rust-prometheus")
    (synopsis "Prometheus instrumentation library for Rust applications.")
    (description "Prometheus instrumentation library for Rust applications.")
    (license license:asl2.0)))

(define-public rust-opentelemetry-prometheus-0.9
  (package
    (name "rust-opentelemetry-prometheus")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-prometheus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qywfsxgn1dfydivki9dr6lh12d45sj40rds9mymwrk62dnc1saf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opentelemetry" ,rust-opentelemetry-0.16)
         ("rust-prometheus" ,rust-prometheus-0.12)
         ("rust-protobuf" ,rust-protobuf-2))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis "Prometheus exporter for OpenTelemetry")
    (description "Prometheus exporter for OpenTelemetry")
    (license license:asl2.0)))

(define-public rust-sluice-0.5
  (package
    (name "rust-sluice")
    (version "0.5.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sluice" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1d9ywr5039ibgaby8sc72f8fs5lpp8j5y6p3npya4jplxz000x3d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-channel" ,rust-async-channel-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3))))
    (home-page "https://github.com/sagebind/sluice")
    (synopsis
      "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (description
      "Efficient ring buffer for byte buffers, FIFO queues, and SPSC channels")
    (license license:expat)))

(define-public rust-num-enum-derive-0.5
  (package
    (name "rust-num-enum-derive")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08dvxpa7l5hx5fcdr0bdv9bzajbcbxsbbnc6hl6zxmwhhiv2p68d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Internal implementation details for ::num_enum (Procedural macros to make inter-operation between primitives and enums easier)")
    (description
      "Internal implementation details for ::num_enum (Procedural macros to make
inter-operation between primitives and enums easier)")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-num-enum-0.5
  (package
    (name "rust-num-enum")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b96nmbhn2gadlh4hna6mz6w892gzp1zic60q1s4akjy0nhkw3bj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-enum-derive" ,rust-num-enum-derive-0.5))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (description
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-rustls-ffi-0.8
  (package
    (name "rust-rustls-ffi")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustls-ffi" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06kqrvm1d5ps9pml26zdd2hm8hh20j6svwvqibpnx7m5rh3jg9cx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-num-enum" ,rust-num-enum-0.5)
         ("rust-rustls" ,rust-rustls-0.20)
         ("rust-rustls-pemfile" ,rust-rustls-pemfile-0.2)
         ("rust-sct" ,rust-sct-0.7)
         ("rust-webpki" ,rust-webpki-0.22))))
    (home-page "https://github.com/rustls/rustls-ffi")
    (synopsis "C-to-rustls bindings")
    (description "C-to-rustls bindings")
    (license (list license:asl2.0 license:isc license:expat))))

(define-public rust-curl-sys-0.4
  (package
    (name "rust-curl-sys")
    (version "0.4.52+curl-7.81.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wc9l9345a31cw2nl4i1lhw01xwmir5jx2bvbgnzv99y0b8w5f0l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-libnghttp2-sys" ,rust-libnghttp2-sys-0.1)
         ("rust-libz-sys" ,rust-libz-sys-1)
         ("rust-mesalink" ,rust-mesalink-1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-rustls-ffi" ,rust-rustls-ffi-0.8)
         ("rust-vcpkg" ,rust-vcpkg-0.2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Native bindings to the libcurl library")
    (description "Native bindings to the libcurl library")
    (license license:expat)))

(define-public rust-curl-0.4
  (package
    (name "rust-curl")
    (version "0.4.42")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fa9v0qxvqxxs1lhb4cs5wpisnxpm5yqvdzqxv65nnyx9s4ppsbx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-curl-sys" ,rust-curl-sys-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-openssl-probe" ,rust-openssl-probe-0.1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-schannel" ,rust-schannel-0.1)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/alexcrichton/curl-rust")
    (synopsis "Rust bindings to libcurl for making HTTP requests")
    (description "Rust bindings to libcurl for making HTTP requests")
    (license license:expat)))

(define-public rust-castaway-0.1
  (package
    (name "rust-castaway")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "castaway" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xhspwy477qy5yg9c3jp713asxckjpx0vfrmz5l7r5zg7naqysd2"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/sagebind/castaway")
    (synopsis
      "Safe, zero-cost downcasting for limited compile-time specialization.")
    (description
      "Safe, zero-cost downcasting for limited compile-time specialization.")
    (license license:expat)))

(define-public rust-async-channel-1
  (package
    (name "rust-async-channel")
    (version "1.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-channel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06b3sq2hd8qwl2xxlc4qalg6xw3l9b41w4sym9g0q70mf93dc511"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-concurrent-queue" ,rust-concurrent-queue-1)
         ("rust-event-listener" ,rust-event-listener-2)
         ("rust-futures-core" ,rust-futures-core-0.3))))
    (home-page "https://github.com/smol-rs/async-channel")
    (synopsis "Async multi-producer multi-consumer channel")
    (description "Async multi-producer multi-consumer channel")
    (license (list license:asl2.0 license:expat))))

(define-public rust-isahc-1
  (package
    (name "rust-isahc")
    (version "1.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "isahc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "127mqv3i70gcwm9k6fsh4lkm9w9ysmy2vqzd2a4kf9fk613yhh6i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-channel" ,rust-async-channel-1)
         ("rust-castaway" ,rust-castaway-0.1)
         ("rust-crossbeam-utils" ,rust-crossbeam-utils-0.8)
         ("rust-curl" ,rust-curl-0.4)
         ("rust-curl-sys" ,rust-curl-sys-0.4)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-event-listener" ,rust-event-listener-2)
         ("rust-futures-lite" ,rust-futures-lite-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-polling" ,rust-polling-2)
         ("rust-publicsuffix" ,rust-publicsuffix-2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-sluice" ,rust-sluice-0.5)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-futures" ,rust-tracing-futures-0.2)
         ("rust-url" ,rust-url-2)
         ("rust-waker-fn" ,rust-waker-fn-1))))
    (home-page "https://github.com/sagebind/isahc")
    (synopsis "The practical HTTP client that is fun to use.")
    (description "The practical HTTP client that is fun to use.")
    (license license:expat)))

(define-public rust-opentelemetry-http-0.5
  (package
    (name "rust-opentelemetry-http")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry-http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wsk140i7ckfgcqhyrynqnmqs8n80yl729ca70zcnxcb1q5yn36m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-isahc" ,rust-isahc-1)
         ("rust-opentelemetry" ,rust-opentelemetry-0.16)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-surf" ,rust-surf-2))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis
      "Helper implementations for exchange of traces and metrics over HTTP")
    (description
      "Helper implementations for exchange of traces and metrics over HTTP")
    (license license:asl2.0)))

(define-public rust-opentelemetry-0.16
  (package
    (name "rust-opentelemetry")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "opentelemetry" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08jxjf86jysg768hqgx7xvhw1gchlipljcn6jgklsv4s9qf9pkz1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-std" ,rust-async-std-1)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
         ("rust-dashmap" ,rust-dashmap-4)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1))))
    (home-page "https://github.com/open-telemetry/opentelemetry-rust")
    (synopsis "A metrics collection and distributed tracing framework")
    (description
      "This package provides a metrics collection and distributed tracing framework")
    (license license:asl2.0)))

(define-public rust-http-0.2
  (package
    (name "rust-http")
    (version "0.2.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00zxqk6m9qksxmlajmhnhgryw6xmqn9riimwx87nz1l4cmscdx1i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-itoa" ,rust-itoa-1))))
    (home-page "https://github.com/hyperium/http")
    (synopsis "A set of types for representing HTTP requests and responses.
")
    (description
      "This package provides a set of types for representing HTTP requests and
responses.")
    (license (list license:expat license:asl2.0))))

(define-public rust-headers-0.3
  (package
    (name "rust-headers")
    (version "0.3.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "headers" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c5ga4rjbjfpw71cl0yb27xrlkrmxyahcsdhv135if7wf42fpi54"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-headers-core" ,rust-headers-core-0.2)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-sha-1" ,rust-sha-1-0.9))))
    (home-page "https://hyper.rs")
    (synopsis "typed HTTP headers")
    (description "typed HTTP headers")
    (license license:expat)))

(define-public rust-futures-task-0.3
  (package
    (name "rust-futures-task")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-task" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wmd3b70sgp1dr3q24439hkm7zj9m1lcafmqvzj7q5ihbi4cdrvf"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Tools for working with tasks.
")
    (description "Tools for working with tasks.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-macro-0.3
  (package
    (name "rust-futures-macro")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0g5xp1xmyfibyscynig2m5gvp5smgg7xvcwr0p3yzc7zvxx99gbd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The futures-rs procedural macro implementations.
")
    (description "The futures-rs procedural macro implementations.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-io-0.3
  (package
    (name "rust-futures-io")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-io" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ckir41haa2hs9znrwavgh33hv3l23jmywqg73xwdam1ym5d7ydi"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
      "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the futures-rs library.
")
    (description
      "The `AsyncRead`, `AsyncWrite`, `AsyncSeek`, and `AsyncBufRead` traits for the
futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-sink-0.3
  (package
    (name "rust-futures-sink")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-sink" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "026m2x353l7x7apa3hdx26ma7kwgxgbghl0393v4zmv8rfn5n1g3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The asynchronous `Sink` trait for the futures-rs library.
")
    (description "The asynchronous `Sink` trait for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-core-0.3
  (package
    (name "rust-futures-core")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mw34nxzggvr2jvk4ljygy077wy32lrdxkyw1j0mj9dqc42gzj6h"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "The core traits and types in for the `futures` library.
")
    (description "The core traits and types in for the `futures` library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-channel-0.3
  (package
    (name "rust-futures-channel")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-channel" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02vzdkc1n25nliwa2758pni7fyn1ch2msrzw18v5ycw8cl5xlgds"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis "Channels for asynchronous communication using futures-rs.
")
    (description "Channels for asynchronous communication using futures-rs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-futures-util-0.3
  (package
    (name "rust-futures-util")
    (version "0.3.19")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "futures-util" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r3i29hhfhv69qjdxh3j4ffxji4hl0yc1gmim1viy9vsni0czdfr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-futures-channel" ,rust-futures-channel-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-futures-macro" ,rust-futures-macro-0.3)
         ("rust-futures-sink" ,rust-futures-sink-0.3)
         ("rust-futures-task" ,rust-futures-task-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-slab" ,rust-slab-0.4)
         ("rust-tokio-io" ,rust-tokio-io-0.1))))
    (home-page "https://rust-lang.github.io/futures-rs")
    (synopsis
      "Common utilities and extension traits for the futures-rs library.
")
    (description
      "Common utilities and extension traits for the futures-rs library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-fluent-pseudo-0.3
  (package
    (name "rust-fluent-pseudo")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-pseudo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0byldssmzjdmynbh1yvdrxcj0xmhqznlmmgwnh8a1fhla7wn5vgx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-regex" ,rust-regex-1))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "Pseudolocalization transformation API for use with Project Fluent API.
")
    (description
      "Pseudolocalization transformation API for use with Project Fluent API.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-self-cell-0.10
  (package
    (name "rust-self-cell")
    (version "0.10.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "self_cell" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1by8h3axgpbiph5nbq80z6a41hd4cqlqc66hgnngs57y42j6by8y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustversion" ,rust-rustversion-1))))
    (home-page "https://github.com/Voultapher/self_cell")
    (synopsis
      "Safe-to-use proc-macro-free self-referential structs in stable Rust.")
    (description
      "Safe-to-use proc-macro-free self-referential structs in stable Rust.")
    (license license:asl2.0)))

(define-public rust-intl-pluralrules-7
  (package
    (name "rust-intl-pluralrules")
    (version "7.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intl_pluralrules" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ksy3hxqs8if3nbvcin0a8390lpkzbk2br1brik70z96hj1ri3xi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinystr" ,rust-tinystr-0.3)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "https://github.com/zbraniecki/pluralrules")
    (synopsis "Unicode Plural Rules categorizer for numeric input.")
    (description "Unicode Plural Rules categorizer for numeric input.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-type-map-0.4
  (package
    (name "rust-type-map")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "type-map" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ilsqq7pcl3k9ggxv2x5fbxxfd6x7ljsndrhc38jmjwnbr63dlxn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-rustc-hash" ,rust-rustc-hash-1))))
    (home-page "https://github.com/kardeiz/type-map")
    (synopsis "Provides a typemap container with FxHashMap")
    (description "This package provides a typemap container with FxHashMap")
    (license (list license:expat license:asl2.0))))

(define-public rust-intl-memoizer-0.5
  (package
    (name "rust-intl-memoizer")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intl-memoizer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vx6cji8ifw77zrgipwmvy1i3v43dcm58hwjxpb1h29i98z46463"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-type-map" ,rust-type-map-0.4)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A memoizer specifically tailored for storing lazy-initialized
intl formatters.
")
    (description
      "This package provides a memoizer specifically tailored for storing
lazy-initialized intl formatters.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fluent-syntax-0.11
  (package
    (name "rust-fluent-syntax")
    (version "0.11.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-syntax" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y6ac7z7sbv51nsa6km5z8rkjj4nvqk91vlghq1ck5c3cjbyvay0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "http://www.projectfluent.org")
    (synopsis "Parser/Serializer tools for Fluent Syntax. 
")
    (description "Parser/Serializer tools for Fluent Syntax. ")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unic-langid-macros-impl-0.9
  (package
    (name "rust-unic-langid-macros-impl")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-macros-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17xaknz6ixhg2cp8x174d54hdlv78akb2s0kw31p8xg2jzynyf99"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-macros-0.9
  (package
    (name "rust-unic-langid-macros")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rfn9pvbypvxyr8iyfx6dycafnn9ih9v8r3dhgr0b23yv3b81y8q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
         ("rust-tinystr" ,rust-tinystr-0.3)
         ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
         ("rust-unic-langid-macros-impl" ,rust-unic-langid-macros-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-tinystr-macros-0.1
  (package
    (name "rust-tinystr-macros")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr-macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c5c361iv5h9brfmhrmz1s7456px5acdd5aqjkazcssfs2playn9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-tinystr" ,rust-tinystr-0.3))))
    (home-page "https://github.com/zbraniecki/tinystr")
    (synopsis "Proc macros for TinyStr.
")
    (description "Proc macros for TinyStr.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tinystr-0.3
  (package
    (name "rust-tinystr")
    (version "0.3.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tinystr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hf74r8qiigddfsxsbkab1pz1hsgi2297azf42k9x39qnknqwwr9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-tinystr-macros" ,rust-tinystr-macros-0.1))))
    (home-page "https://github.com/zbraniecki/tinystr")
    (synopsis "A small ASCII-only bounded length string representation.
")
    (description
      "This package provides a small ASCII-only bounded length string representation.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-unic-langid-impl-0.9
  (package
    (name "rust-unic-langid-impl")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid-impl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0kck3fpdvqv5nha47xkna3zsr8ik9hpyr5ac830n4j29y3m8wjhs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tinystr" ,rust-tinystr-0.3))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-0.9
  (package
    (name "rust-unic-langid")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unic-langid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rcw8llr3a120qad7rlbv4fb19l744ckxwnx37dhn0qafg6qyckk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
         ("rust-unic-langid-macros" ,rust-unic-langid-macros-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description "API for managing Unicode Language Identifiers")
    (license (list license:expat license:asl2.0))))

(define-public rust-fluent-langneg-0.13
  (package
    (name "rust-fluent-langneg")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-langneg" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "152yxplc11vmxkslvmaqak9x86xnavnhdqyhrh38ym37jscd0jic"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://projectfluent.org/")
    (synopsis "A library for language and locale negotiation.
")
    (description
      "This package provides a library for language and locale negotiation.")
    (license license:asl2.0)))

(define-public rust-fluent-bundle-0.15
  (package
    (name "rust-fluent-bundle")
    (version "0.15.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent-bundle" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zbzm13rfz7fay7bps7jd4j1pdnlxmdzzfymyq2iawf9vq0wchp2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fluent-langneg" ,rust-fluent-langneg-0.13)
         ("rust-fluent-syntax" ,rust-fluent-syntax-0.11)
         ("rust-intl-memoizer" ,rust-intl-memoizer-0.5)
         ("rust-intl-pluralrules" ,rust-intl-pluralrules-7)
         ("rust-rustc-hash" ,rust-rustc-hash-1)
         ("rust-self-cell" ,rust-self-cell-0.10)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A localization system designed to unleash the entire expressive power of
natural language translations.
")
    (description
      "This package provides a localization system designed to unleash the entire
expressive power of natural language translations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fluent-0.16
  (package
    (name "rust-fluent")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fluent" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19s7z0gw95qdsp9hhc00xcy11nwhnx93kknjmdvdnna435w97xk1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fluent-bundle" ,rust-fluent-bundle-0.15)
         ("rust-fluent-pseudo" ,rust-fluent-pseudo-0.3)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "http://www.projectfluent.org")
    (synopsis
      "A localization system designed to unleash the entire expressive power of
natural language translations.
")
    (description
      "This package provides a localization system designed to unleash the entire
expressive power of natural language translations.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-chacha20-0.7
  (package
    (name "rust-chacha20")
    (version "0.7.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chacha20" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c8h4sp9zh13v8p9arydjcj92xc6j3mccrjc4mizrvq7fzx9717h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-rand-core" ,rust-rand-core-0.6)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/stream-ciphers")
    (synopsis
      "The ChaCha20 stream cipher (RFC 8439) implemented in pure Rust using traits
from the RustCrypto `cipher` crate, with optional architecture-specific
hardware acceleration (AVX2, SSE2). Additionally provides the ChaCha8, ChaCha12,
XChaCha20, XChaCha12 and XChaCha8 stream ciphers, and also optional
rand_core-compatible RNGs based on those ciphers.
")
    (description
      "The ChaCha20 stream cipher (RFC 8439) implemented in pure Rust using traits from
the RustCrypto `cipher` crate, with optional architecture-specific hardware
acceleration (AVX2, SSE2).  Additionally provides the ChaCha8, ChaCha12,
XChaCha20, XChaCha12 and XChaCha8 stream ciphers, and also optional
rand_core-compatible RNGs based on those ciphers.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-chacha20poly1305-0.8
  (package
    (name "rust-chacha20poly1305")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "chacha20poly1305" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18mb6k1w71dqv5q50an4rvp19l6yg8ssmvfrmknjfh2z0az7lm5n"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-chacha20" ,rust-chacha20-0.7)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-poly1305" ,rust-poly1305-0.7)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/AEADs")
    (synopsis
      "Pure Rust implementation of the ChaCha20Poly1305 Authenticated Encryption
with Additional Data Cipher (RFC 8439) with optional architecture-specific
hardware acceleration. Also contains implementations of the XChaCha20Poly1305
extended nonce variant of ChaCha20Poly1305, and the reduced-round
ChaCha8Poly1305 and ChaCha12Poly1305 lightweight variants.
")
    (description
      "Pure Rust implementation of the ChaCha20Poly1305 Authenticated Encryption with
Additional Data Cipher (RFC 8439) with optional architecture-specific hardware
acceleration.  Also contains implementations of the XChaCha20Poly1305 extended
nonce variant of ChaCha20Poly1305, and the reduced-round ChaCha8Poly1305 and
ChaCha12Poly1305 lightweight variants.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-csrf-0.4
  (package
    (name "rust-csrf")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csrf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1q7ixhshj6a7x2vgsr4d4iqa5mgp4fwkr4lx2hgvnj9xcy1py9dh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-aes-gcm" ,rust-aes-gcm-0.9)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-chacha20poly1305" ,rust-chacha20poly1305-0.8)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-generic-array" ,rust-generic-array-0.14)
         ("rust-hmac" ,rust-hmac-0.11)
         ("rust-log" ,rust-log-0.4)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha2" ,rust-sha2-0.9)
         ("rust-typemap" ,rust-typemap-0.3))))
    (home-page "https://github.com/heartsucker/rust-csrf")
    (synopsis "CSRF protection primitives")
    (description "CSRF protection primitives")
    (license license:expat)))

(define-public rust-hkdf-0.12
  (package
    (name "rust-hkdf")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hkdf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lcmyrl1iyzp7ii1w3mjqnxsjhjrsnm61fcpfjshbz5nfyf1xx4l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-hmac" ,rust-hmac-0.12))))
    (home-page "https://github.com/RustCrypto/KDFs/")
    (synopsis "HMAC-based Extract-and-Expand Key Derivation Function (HKDF)")
    (description
      "HMAC-based Extract-and-Expand Key Derivation Function (HKDF)")
    (license (list license:expat license:asl2.0))))

(define-public rust-zeroize-1
  (package
    (name "rust-zeroize")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zeroize" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1z8yix823b6lz878qwg6bvwhg3lb0cbw3c9yij9p8mbv7zdzfmj7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-zeroize-derive" ,rust-zeroize-derive-1))))
    (home-page "https://github.com/RustCrypto/utils/tree/master/zeroize")
    (synopsis
      "Securely clear secrets from memory with a simple trait built on
stable Rust primitives which guarantee memory is zeroed using an
operation will not be 'optimized away' by the compiler.
Uses a portable pure Rust implementation that works everywhere,
even WASM!
")
    (description
      "Securely clear secrets from memory with a simple trait built on stable Rust
primitives which guarantee memory is zeroed using an operation will not be
'optimized away' by the compiler.  Uses a portable pure Rust implementation that
works everywhere, even WASM!")
    (license (list license:asl2.0 license:expat))))

(define-public rust-polyval-0.5
  (package
    (name "rust-polyval")
    (version "0.5.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "polyval" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1890wqvc0csc9y9k9k4gsbz91rgdnhn6xnfmy9pqkh674fvd46c4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-universal-hash" ,rust-universal-hash-0.4)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/universal-hashes")
    (synopsis
      "POLYVAL is a GHASH-like universal hash over GF(2^128) useful for constructing
a Message Authentication Code (MAC)
")
    (description
      "POLYVAL is a GHASH-like universal hash over GF(2^128) useful for constructing a
Message Authentication Code (MAC)")
    (license (list license:asl2.0 license:expat))))

(define-public rust-ghash-0.4
  (package
    (name "rust-ghash")
    (version "0.4.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ghash" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "169wvrc2k9lw776x3pmqp76kc0w5717wz01bfg9rz0ypaqbcr0qm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-polyval" ,rust-polyval-0.5)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/universal-hashes")
    (synopsis
      "Universal hash over GF(2^128) useful for constructing a Message Authentication Code (MAC),
as in the AES-GCM authenticated encryption cipher.
")
    (description
      "Universal hash over GF(2^128) useful for constructing a Message Authentication
Code (MAC), as in the AES-GCM authenticated encryption cipher.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-aead-0.4
  (package
    (name "rust-aead")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aead" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xw8kp9j1whfdxhgmr2qf9xgslkg52zh6gzmhsh13y9w3s73nq8b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-blobby" ,rust-blobby-0.3)
         ("rust-generic-array" ,rust-generic-array-0.14)
         ("rust-heapless" ,rust-heapless-0.7)
         ("rust-rand-core" ,rust-rand-core-0.6))))
    (home-page "https://github.com/RustCrypto/traits")
    (synopsis
      "Traits for Authenticated Encryption with Associated Data (AEAD) algorithms,
such as AES-GCM as ChaCha20Poly1305, which provide a high-level API
")
    (description
      "Traits for Authenticated Encryption with Associated Data (AEAD) algorithms, such
as AES-GCM as ChaCha20Poly1305, which provide a high-level API")
    (license (list license:expat license:asl2.0))))

(define-public rust-aes-gcm-0.9
  (package
    (name "rust-aes-gcm")
    (version "0.9.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "aes-gcm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xndncn1phjb7pjam63vl0yp7h8jh95m0yxanr1092vx7al8apyz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aead" ,rust-aead-0.4)
         ("rust-aes" ,rust-aes-0.7)
         ("rust-cipher" ,rust-cipher-0.3)
         ("rust-ctr" ,rust-ctr-0.8)
         ("rust-ghash" ,rust-ghash-0.4)
         ("rust-subtle" ,rust-subtle-2)
         ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://github.com/RustCrypto/AEADs")
    (synopsis
      "Pure Rust implementation of the AES-GCM (Galois/Counter Mode)
Authenticated Encryption with Associated Data (AEAD) Cipher
with optional architecture-specific hardware acceleration
")
    (description
      "Pure Rust implementation of the AES-GCM (Galois/Counter Mode) Authenticated
Encryption with Associated Data (AEAD) Cipher with optional
architecture-specific hardware acceleration")
    (license (list license:asl2.0 license:expat))))

(define-public rust-cookie-0.16
  (package
    (name "rust-cookie")
    (version "0.16.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cookie" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01fa6z8sqqg19ya0l9ifh8vn05l5hpxdzkbh489mpymhw5np1m4l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aes-gcm" ,rust-aes-gcm-0.9)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-hkdf" ,rust-hkdf-0.12)
         ("rust-hmac" ,rust-hmac-0.12)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-sha2" ,rust-sha2-0.10)
         ("rust-subtle" ,rust-subtle-2)
         ("rust-time" ,rust-time-0.3)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/SergioBenitez/cookie-rs")
    (synopsis
      "HTTP cookie parsing and cookie jar management. Supports signed and private
(encrypted, authenticated) jars.
")
    (description
      "HTTP cookie parsing and cookie jar management.  Supports signed and private
(encrypted, authenticated) jars.")
    (license (list license:expat license:asl2.0))))

(define-public rust-zstd-sys-1
  (package
    (name "rust-zstd-sys")
    (version "1.5.0+zstd.1.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rf4pnp4palnmh846n7s511q6apfajq33dy6bw16j0r4811hjv2f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.57)
         ("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Low-level bindings for the zstd compression library.")
    (description "Low-level bindings for the zstd compression library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-zstd-safe-3
  (package
    (name "rust-zstd-safe")
    (version "3.1.0+zstd.1.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd-safe" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vb6zc4fpigafd6ln4s8p5avaggpva3m6mchw03f8zxd4dk958as"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2) ("rust-zstd-sys" ,rust-zstd-sys-1))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Safe low-level bindings for the zstd compression library.")
    (description "Safe low-level bindings for the zstd compression library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-zstd-0.7
  (package
    (name "rust-zstd")
    (version "0.7.0+zstd.1.4.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "zstd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0lndq8ygzyz4c0n2vjhwfxnasyd132jrwxxzn4ajwdyqh4j7aa4l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures" ,rust-futures-0.1)
         ("rust-tokio-io" ,rust-tokio-io-0.1)
         ("rust-zstd-safe" ,rust-zstd-safe-3))))
    (home-page "https://github.com/gyscos/zstd-rs")
    (synopsis "Binding for the zstd compression library.")
    (description "Binding for the zstd compression library.")
    (license license:expat)))

(define-public rust-async-compression-0.3
  (package
    (name "rust-async-compression")
    (version "0.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-compression" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19kkhh9wsr6jcrzfkc3brl4ph8qz1sj2visz0nqs4x034yxwqhsl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-brotli" ,rust-brotli-3)
         ("rust-bytes" ,rust-bytes-0.5)
         ("rust-bzip2" ,rust-bzip2-0.4)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-io" ,rust-futures-io-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-0.2)
         ("rust-tokio" ,rust-tokio-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-xz2" ,rust-xz2-0.1)
         ("rust-zstd" ,rust-zstd-0.7)
         ("rust-zstd-safe" ,rust-zstd-safe-3))))
    (home-page "https://github.com/Nemo157/async-compression")
    (synopsis
      "Adaptors between compression crates and Rust's modern asynchronous IO types.
")
    (description
      "Adaptors between compression crates and Rust's modern asynchronous IO types.")
    (license (list license:expat license:asl2.0))))

(define-public rust-poem-1
  (package
    (name "rust-poem")
    (version "1.2.43")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "poem" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jpcghrvwx0vc2casnnjygiab1xwxa347ccq422akmdyazxn30cy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-cookie" ,rust-cookie-0.16)
         ("rust-csrf" ,rust-csrf-0.4)
         ("rust-fluent" ,rust-fluent-0.16)
         ("rust-fluent-langneg" ,rust-fluent-langneg-0.13)
         ("rust-fluent-syntax" ,rust-fluent-syntax-0.11)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-httpdate" ,rust-httpdate-1)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-intl-memoizer" ,rust-intl-memoizer-0.5)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-multer" ,rust-multer-2)
         ("rust-opentelemetry" ,rust-opentelemetry-0.16)
         ("rust-opentelemetry-http" ,rust-opentelemetry-http-0.5)
         ("rust-opentelemetry-prometheus" ,rust-opentelemetry-prometheus-0.9)
         ("rust-opentelemetry-semantic-conventions"
          ,rust-opentelemetry-semantic-conventions-0.8)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-poem-derive" ,rust-poem-derive-1)
         ("rust-priority-queue" ,rust-priority-queue-1)
         ("rust-prometheus" ,rust-prometheus-0.12)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-redis" ,rust-redis-0.21)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-sse-codec" ,rust-sse-codec-0.3)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.15)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-typed-headers" ,rust-typed-headers-0.2)
         ("rust-unic-langid" ,rust-unic-langid-0.9))))
    (home-page "https://github.com/poem-web/poem")
    (synopsis
      "Poem is a full-featured and easy-to-use web framework with the Rust programming language.")
    (description
      "Poem is a full-featured and easy-to-use web framework with the Rust programming
language.")
    (license (list license:expat license:asl2.0))))

(define-public rust-iri-string-0.4
  (package
    (name "rust-iri-string")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "iri-string" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y2z4f5y87hnff2d5lcl811hp7iv2f5qri7x3fgm48z2q4w7c3wg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-nom" ,rust-nom-7) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/lo48576/iri-string")
    (synopsis "IRI as string types")
    (description "IRI as string types")
    (license (list license:expat license:asl2.0))))

(define-public rust-tower-http-0.1
  (package
    (name "rust-tower-http")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tower-http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1d8b8b5gk82mhjrmwvpz1b3g2dilqabz5rj1zaxm90758wkagv41"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-compression" ,rust-async-compression-0.3)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-iri-string" ,rust-iri-string-0.4)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3)
         ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://github.com/tower-rs/tower-http")
    (synopsis "Tower middleware and utilities for HTTP clients and servers")
    (description "Tower middleware and utilities for HTTP clients and servers")
    (license license:expat)))

(define-public rust-tungstenite-0.14
  (package
    (name "rust-tungstenite")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1db5j4792b197v95y7hr8j9n0qn75rdc1xcd19mjfbmxi9axicm0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-base64" ,rust-base64-0.13)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-rustls" ,rust-rustls-0.19)
         ("rust-rustls-native-certs" ,rust-rustls-native-certs-0.5)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-url" ,rust-url-2)
         ("rust-utf-8" ,rust-utf-8-0.7)
         ("rust-webpki" ,rust-webpki-0.21))))
    (home-page "https://github.com/snapview/tungstenite-rs")
    (synopsis "Lightweight stream-based WebSocket implementation")
    (description "Lightweight stream-based WebSocket implementation")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-tungstenite-0.15
  (package
    (name "rust-tokio-tungstenite")
    (version "0.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-tungstenite" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1n6b8qgjgnxl5g1fwmsfcgmshpv814yhqja56nc9h75gbkwf67ai"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-native-tls" ,rust-native-tls-0.2)
         ("rust-pin-project" ,rust-pin-project-1)
         ("rust-rustls" ,rust-rustls-0.19)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-native-tls" ,rust-tokio-native-tls-0.3)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-tungstenite" ,rust-tungstenite-0.14)
         ("rust-webpki" ,rust-webpki-0.21)
         ("rust-webpki-roots" ,rust-webpki-roots-0.21))))
    (home-page "https://github.com/snapview/tokio-tungstenite")
    (synopsis
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket implementation")
    (description
      "Tokio binding for Tungstenite, the Lightweight stream-based WebSocket
implementation")
    (license license:expat)))

(define-public rust-sync-wrapper-0.1
  (package
    (name "rust-sync-wrapper")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sync_wrapper" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1a59lwsw52d1a64l2y1m7npfw6xjvrjf96c5014g1b69lkj8yl90"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://docs.rs/sync_wrapper")
    (synopsis
      "A tool for enlisting the compilerâ\x80\x99s help in proving the absence of concurrency")
    (description
      "This package provides a tool for enlisting the compilerâ\x80\x99s help in proving the
absence of concurrency")
    (license license:asl2.0)))

(define-public rust-sha-1-0.9
  (package
    (name "rust-sha-1")
    (version "0.9.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha-1" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19jibp8l9k5v4dnhj5kfhaczdfd997h22qz0hin6pw9wvc9ngkcr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-block-buffer" ,rust-block-buffer-0.9)
         ("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-cpufeatures" ,rust-cpufeatures-0.2)
         ("rust-digest" ,rust-digest-0.9)
         ("rust-opaque-debug" ,rust-opaque-debug-0.3)
         ("rust-sha1-asm" ,rust-sha1-asm-0.5))))
    (home-page "https://github.com/RustCrypto/hashes")
    (synopsis "SHA-1 hash function")
    (description "SHA-1 hash function")
    (license (list license:expat license:asl2.0))))

(define-public rust-axum-0.2
  (package
    (name "rust-axum")
    (version "0.2.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "axum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wisxqw4inm2sk7wzvy5njnwh63wlmx0948lvc4p50if41dgj24g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-base64" ,rust-base64-0.13)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-headers" ,rust-headers-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-http-body" ,rust-http-body-0.4)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-multer" ,rust-multer-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-urlencoded" ,rust-serde-urlencoded-0.7)
         ("rust-sha-1" ,rust-sha-1-0.9)
         ("rust-sync-wrapper" ,rust-sync-wrapper-0.1)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-tungstenite" ,rust-tokio-tungstenite-0.15)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-tower" ,rust-tower-0.4)
         ("rust-tower-http" ,rust-tower-http-0.1)
         ("rust-tower-layer" ,rust-tower-layer-0.3)
         ("rust-tower-service" ,rust-tower-service-0.3))))
    (home-page "https://github.com/tokio-rs/axum")
    (synopsis "Web framework that focuses on ergonomics and modularity")
    (description "Web framework that focuses on ergonomics and modularity")
    (license license:expat)))

(define-public rust-rust-embed-6
  (package
    (name "rust-rust-embed")
    (version "6.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-embed" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00p6zb6k9x33nxhb5w12ap75ca2wgz4kmdyx50gfivncz2zpf0yl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-actix-web" ,rust-actix-web-3)
         ("rust-axum" ,rust-axum-0.2)
         ("rust-hex" ,rust-hex-0.4)
         ("rust-include-flate" ,rust-include-flate-0.1)
         ("rust-mime-guess" ,rust-mime-guess-2)
         ("rust-poem" ,rust-poem-1)
         ("rust-rocket" ,rust-rocket-0.4)
         ("rust-rust-embed-impl" ,rust-rust-embed-impl-6)
         ("rust-rust-embed-utils" ,rust-rust-embed-utils-7)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-warp" ,rust-warp-0.3))))
    (home-page "https://github.com/pyros2097/rust-embed")
    (synopsis
      "Rust Custom Derive Macro which loads files into the rust binary at compile time during release and loads the file from the fs during dev")
    (description
      "Rust Custom Derive Macro which loads files into the rust binary at compile time
during release and loads the file from the fs during dev")
    (license license:expat)))

(define-public rust-smartstring-0.2
  (package
    (name "rust-smartstring")
    (version "0.2.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smartstring" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16rc6n0p4r4aw6k6jxf2s37wyaijaa4pwpw7rqki7cn2q0qnmaii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-0.4)
         ("rust-proptest" ,rust-proptest-0.10)
         ("rust-serde" ,rust-serde-1)
         ("rust-static-assertions" ,rust-static-assertions-1))))
    (home-page "https://github.com/bodil/smartstring")
    (synopsis "Compact inlined strings")
    (description "Compact inlined strings")
    (license unknown-license!)))

(define-public rust-smallvec-1
  (package
    (name "rust-smallvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10zf4fn63p2d6sx8qap3jvyarcfw563308x3431hd4c34r35gpgj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/servo/rust-smallvec")
    (synopsis
      "'Small vector' optimization: store up to a small number of items on the stack")
    (description
      "'Small vector' optimization: store up to a small number of items on the stack")
    (license (list license:expat license:asl2.0))))

(define-public rust-ubyte-0.10
  (package
    (name "rust-ubyte")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ubyte" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zp8x55w57dkcy20vnc50izsjcgz7mj9b0d9z3i5v188wywnnxa2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/SergioBenitez/ubyte")
    (synopsis
      "A simple, complete, const-everything, saturating, human-friendly, no_std library for byte units.
")
    (description
      "This package provides a simple, complete, const-everything, saturating,
human-friendly, no_std library for byte units.")
    (license (list license:expat license:asl2.0))))

(define-public rust-tokio-stream-0.1
  (package
    (name "rust-tokio-stream")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tokio-stream" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qwq0y21xprsql4v9y1cm1ymhgw66rznjmnjrjsii27zxy25852h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities to work with `Stream` and `tokio`.
")
    (description "Utilities to work with `Stream` and `tokio`.")
    (license license:expat)))

(define-public rust-tracing-attributes-0.1
  (package
    (name "rust-tracing-attributes")
    (version "0.1.18")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing-attributes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13izzsgrmcg2076ksdibvyxx7d8y9klm3b9pycjyh4hmz2w81x7l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://tokio.rs")
    (synopsis
      "Procedural macro attributes for automatically instrumenting functions.
")
    (description
      "Procedural macro attributes for automatically instrumenting functions.")
    (license license:expat)))

(define-public rust-tracing-0.1
  (package
    (name "rust-tracing")
    (version "0.1.29")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "tracing" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0191zcbnkn8wy0b7xbz7jd9m2xf3sjr8k3cfqzghxwya6a966nip"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-tracing-attributes" ,rust-tracing-attributes-0.1)
         ("rust-tracing-core" ,rust-tracing-core-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Application-level tracing for Rust.
")
    (description "Application-level tracing for Rust.")
    (license license:expat)))

(define-public rust-generator-0.7
  (package
    (name "rust-generator")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generator" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vhj3f0rf4mlh5vz7pz5rxmgry1cc62x21mf9ld1r292m2f2gnf1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/Xudong-Huang/generator-rs.git")
    (synopsis "Stackfull Generator Library in Rust")
    (description "Stackfull Generator Library in Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-loom-0.5
  (package
    (name "rust-loom")
    (version "0.5.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "loom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02a30cv9l2afjq5bg42hgcjspx8fgwyij0cf9saw8b73539wgigd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-generator" ,rust-generator-0.7)
         ("rust-pin-utils" ,rust-pin-utils-0.1)
         ("rust-scoped-tls" ,rust-scoped-tls-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tracing" ,rust-tracing-0.1)
         ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3))))
    (home-page "https://github.com/tokio-rs/loom")
    (synopsis "Permutation testing for concurrent code")
    (description "Permutation testing for concurrent code")
    (license license:expat)))

(define-public rust-state-0.5
  (package
    (name "rust-state")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "state" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1rbd5zg3zsj95di88h4my346llaiyj89cp1nbr5h9lz6d59lzkw7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-loom" ,rust-loom-0.5))))
    (home-page "https://github.com/SergioBenitez/state")
    (synopsis
      "A library for safe and effortless global and thread-local state management.
")
    (description
      "This package provides a library for safe and effortless global and thread-local
state management.")
    (license (list license:expat license:asl2.0))))

(define-public rust-stable-pattern-0.1
  (package
    (name "rust-stable-pattern")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stable-pattern" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i8hq82vm82mqj02qqcsd7caibrih7x5w3a1xpm8hpv30261cr25"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-memchr" ,rust-memchr-2))))
    (home-page "https://github.com/SergioBenitez/stable-pattern")
    (synopsis "Stable port of std::str::Pattern and friends.")
    (description "Stable port of std::str::Pattern and friends.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-http-0.5
  (package
    (name "rust-rocket-http")
    (version "0.5.0-rc.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket_http" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gayw8bglx9kyph9xk76k55q8wvmwv6r1rgb2qisrz6j2bavgj13"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cookie" ,rust-cookie-0.15)
         ("rust-either" ,rust-either-1)
         ("rust-http" ,rust-http-0.2)
         ("rust-hyper" ,rust-hyper-0.14)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-pear" ,rust-pear-0.2)
         ("rust-percent-encoding" ,rust-percent-encoding-2)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-ref-cast" ,rust-ref-cast-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-stable-pattern" ,rust-stable-pattern-0.1)
         ("rust-state" ,rust-state-0.5)
         ("rust-time" ,rust-time-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-rustls" ,rust-tokio-rustls-0.22)
         ("rust-uncased" ,rust-uncased-0.9)
         ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://rocket.rs")
    (synopsis
      "Types, traits, and parsers for HTTP requests, responses, and headers.
")
    (description
      "Types, traits, and parsers for HTTP requests, responses, and headers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-core-0.3
  (package
    (name "rust-devise-core")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise_core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l00qiih4z14ai0c3s16nlvw0kv4p07ygi6a0ms0knc78xpz87l4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro2-diagnostics" ,rust-proc-macro2-diagnostics-0.9)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-codegen-0.3
  (package
    (name "rust-devise-codegen")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cp7nnfwvjp6wfq11n0ffjjrwfa1wbsb58g1bz3ha6z5lvkp6g0j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise-core" ,rust-devise-core-0.3)
         ("rust-quote" ,rust-quote-1))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-devise-0.3
  (package
    (name "rust-devise")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "devise" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "15dmibnykic2a1ndi66shyvxmpfysnhf05lg2iv8871g0w5miish"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise-codegen" ,rust-devise-codegen-0.3)
         ("rust-devise-core" ,rust-devise-core-0.3))))
    (home-page "https://github.com/SergioBenitez/Devise")
    (synopsis "A library for devising derives and other procedural macros.")
    (description
      "This package provides a library for devising derives and other procedural
macros.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-codegen-0.5
  (package
    (name "rust-rocket-codegen")
    (version "0.5.0-rc.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fx8mr2ybxvsjnqnz9wzh1cvvfvlsz2if33im2xmifby5x3gmxb6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-devise" ,rust-devise-0.3)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-rocket-http" ,rust-rocket-http-0.5)
         ("rust-syn" ,rust-syn-1)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://rocket.rs")
    (synopsis "Procedural macros for the Rocket web framework.")
    (description "Procedural macros for the Rocket web framework.")
    (license (list license:expat license:asl2.0))))

(define-public rust-multer-2
  (package
    (name "rust-multer")
    (version "2.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "multer" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0diqknyfg0m131bm19rll4abg34ad7k122arcwb5q7anhzk3b3sz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytes" ,rust-bytes-1)
         ("rust-encoding-rs" ,rust-encoding-rs-0.8)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-http" ,rust-http-0.2)
         ("rust-httparse" ,rust-httparse-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-mime" ,rust-mime-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-spin" ,rust-spin-0.9)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/rousan/multer-rs")
    (synopsis
      "An async parser for `multipart/form-data` content-type in Rust.")
    (description
      "An async parser for `multipart/form-data` content-type in Rust.")
    (license license:expat)))

(define-public rust-proc-macro2-diagnostics-0.9
  (package
    (name "rust-proc-macro2-diagnostics")
    (version "0.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proc-macro2-diagnostics" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nmazlb1dkznjds7qwms7yxhi33ajc3isji2lsgx8r3lsqk9gwjb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "")
    (synopsis "Diagnostics for proc-macro2.")
    (description "Diagnostics for proc-macro2.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pear-codegen-0.2
  (package
    (name "rust-pear-codegen")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pear_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1l4209fi1n0wj110l12l4xpy32d1xffm61nm82vyq0r37ijcm9c2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-proc-macro2-diagnostics" ,rust-proc-macro2-diagnostics-0.9)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "A (codegen) pear is a fruit.")
    (description "This package provides a (codegen) pear is a fruit.")
    (license (list license:expat license:asl2.0))))

(define-public rust-inlinable-string-0.1
  (package
    (name "rust-inlinable-string")
    (version "0.1.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "inlinable_string" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ysjci8yfvxgf51z0ny2nnwhxrclhmb3vbngin8v4bznhr3ybyn8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/fitzgen/inlinable_string")
    (synopsis
      "The `inlinable_string` crate provides the `InlinableString` type -- an owned, grow-able UTF-8 string that stores small strings inline and avoids heap-allocation -- and the `StringExt` trait which abstracts string operations over both `std::string::String` and `InlinableString` (or even your own custom string type).")
    (description
      "The `inlinable_string` crate provides the `InlinableString` type -- an owned,
grow-able UTF-8 string that stores small strings inline and avoids
heap-allocation -- and the `StringExt` trait which abstracts string operations
over both `std::string::String` and `InlinableString` (or even your own custom
string type).")
    (license (list license:asl2.0 license:expat))))

(define-public rust-pear-0.2
  (package
    (name "rust-pear")
    (version "0.2.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pear" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "00l7llav8cidhclx0m2gxm267pfa90c7r2x7xbinij74qm0l5r0m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-inlinable-string" ,rust-inlinable-string-0.1)
         ("rust-pear-codegen" ,rust-pear-codegen-0.2)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "")
    (synopsis "A pear is a fruit.")
    (description "This package provides a pear is a fruit.")
    (license (list license:expat license:asl2.0))))

(define-public rust-figment-0.10
  (package
    (name "rust-figment")
    (version "0.10.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "figment" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1pr2w6pldkkjavj1sacn9xiibzhlf13ply0gnnxan616qy9442vr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atomic" ,rust-atomic-0.5)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-pear" ,rust-pear-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-yaml" ,rust-serde-yaml-0.8)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-uncased" ,rust-uncased-0.9)
         ("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/SergioBenitez/Figment")
    (synopsis "A configuration library so con-free, it's unreal.")
    (description
      "This package provides a configuration library so con-free, it's unreal.")
    (license (list license:expat license:asl2.0))))

(define-public rust-binascii-0.1
  (package
    (name "rust-binascii")
    (version "0.1.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "binascii" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wnaglgl72pn5ilv61q6y34w76gbg7crb8ifqk6lsxnq2gajjg9q"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/naim94a/binascii-rs")
    (synopsis
      "Useful no-std binascii operations including base64, base32 and base16 (hex)")
    (description
      "Useful no-std binascii operations including base64, base32 and base16 (hex)")
    (license license:expat)))

(define-public rust-atomic-0.5
  (package
    (name "rust-atomic")
    (version "0.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "atomic" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0k135q1qfmxxyzrlhr47r0j38r5fnd4163rgl552qxyagrk853dq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-autocfg" ,rust-autocfg-1))))
    (home-page "https://github.com/Amanieu/atomic-rs")
    (synopsis "Generic Atomic<T> wrapper type")
    (description "Generic Atomic<T> wrapper type")
    (license (list license:asl2.0 license:expat))))

(define-public rust-async-trait-0.1
  (package
    (name "rust-async-trait")
    (version "0.1.52")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "async-trait" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qv5l9izrd96zcrlcr4x1kh41ylq1d9702f3vl8w11m2rb67l6h6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/dtolnay/async-trait")
    (synopsis "Type erasure for async trait methods")
    (description "Type erasure for async trait methods")
    (license (list license:expat license:asl2.0))))

(define-public rust-rocket-0.5
  (package
    (name "rust-rocket")
    (version "0.5.0-rc.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rocket" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wni9nf9f0d7hvsalh9adybff4dd1npir0qn72zibsx08a6c2w8a"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-async-stream" ,rust-async-stream-0.3)
         ("rust-async-trait" ,rust-async-trait-0.1)
         ("rust-atomic" ,rust-atomic-0.5)
         ("rust-atty" ,rust-atty-0.2)
         ("rust-binascii" ,rust-binascii-0.1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-either" ,rust-either-1)
         ("rust-figment" ,rust-figment-0.10)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-indexmap" ,rust-indexmap-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-multer" ,rust-multer-2)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-parking-lot" ,rust-parking-lot-0.11)
         ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
         ("rust-rand" ,rust-rand-0.8)
         ("rust-ref-cast" ,rust-ref-cast-1)
         ("rust-rmp-serde" ,rust-rmp-serde-0.15)
         ("rust-rocket-codegen" ,rust-rocket-codegen-0.5)
         ("rust-rocket-http" ,rust-rocket-http-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-state" ,rust-state-0.5)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-time" ,rust-time-0.2)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-tokio-stream" ,rust-tokio-stream-0.1)
         ("rust-tokio-util" ,rust-tokio-util-0.6)
         ("rust-ubyte" ,rust-ubyte-0.10)
         ("rust-uuid" ,rust-uuid-0.8)
         ("rust-version-check" ,rust-version-check-0.9)
         ("rust-yansi" ,rust-yansi-0.5)
         ("rust-yansi" ,rust-yansi-0.5))))
    (home-page "https://rocket.rs")
    (synopsis
      "Web framework with a focus on usability, security, extensibility, and speed.
")
    (description
      "Web framework with a focus on usability, security, extensibility, and speed.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rust-decimal-1
  (package
    (name "rust-rust-decimal")
    (version "1.20.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust_decimal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gqyw7xqhbbjvs5152760s739ccphdz93pmkmzfh0f3yczj3qng0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1)
         ("rust-arrayvec" ,rust-arrayvec-0.7)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-bytes" ,rust-bytes-1)
         ("rust-diesel" ,rust-diesel-1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-postgres" ,rust-postgres-0.19)
         ("rust-rocket" ,rust-rocket-0.5)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-tokio-postgres" ,rust-tokio-postgres-0.7))))
    (home-page "https://github.com/paupino/rust-decimal")
    (synopsis
      "A Decimal Implementation written in pure Rust suitable for financial calculations.")
    (description
      "This package provides a Decimal Implementation written in pure Rust suitable for
financial calculations.")
    (license license:expat)))

(define-public rust-rhai-codegen-1
  (package
    (name "rust-rhai-codegen")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rhai_codegen" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c99iw6w9y7ncl6dlnj4fzzjvz9l46sxb642mirfra3sdbbk6bg0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://rhai.rs/book/plugins/index.html")
    (synopsis
      "Procedural macros support package for Rhai, a scripting language and engine for Rust")
    (description
      "Procedural macros support package for Rhai, a scripting language and engine for
Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-hashbrown-0.8
  (package
    (name "rust-hashbrown")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hashbrown" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09cckr5l71ypvfdbvv1qsag4222blixwn9300hpbr831j3vn46z9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.3)
         ("rust-autocfg" ,rust-autocfg-1)
         ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-rustc-std-workspace-alloc" ,rust-rustc-std-workspace-alloc-1)
         ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-lang/hashbrown")
    (synopsis "A Rust port of Google's SwissTable hash map")
    (description
      "This package provides a Rust port of Google's SwissTable hash map")
    (license (list license:asl2.0 license:expat))))

(define-public rust-no-std-compat-0.4
  (package
    (name "rust-no-std-compat")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "no-std-compat" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "132vrf710zsdp40yp1z3kgc2ss8pi0z4gmihsz3y7hl4dpd56f5r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hashbrown" ,rust-hashbrown-0.8) ("rust-spin" ,rust-spin-0.5))))
    (home-page "https://gitlab.com/jD91mZM2/no-std-compat")
    (synopsis
      "A `#![no_std]` compatibility layer that will make porting your crate to no_std *easy*.")
    (description
      "This package provides a `#![no_std]` compatibility layer that will make porting
your crate to no_std *easy*.")
    (license license:expat)))

(define-public rust-instant-0.1
  (package
    (name "rust-instant")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "instant" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b2bx5qdlwayriidhrag8vhy10kdfimfhmb3jnjmsz2h9j1bwnvs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-js-sys" ,rust-js-sys-0.3)
         ("rust-stdweb" ,rust-stdweb-0.4)
         ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
         ("rust-web-sys" ,rust-web-sys-0.3))))
    (home-page "https://github.com/sebcrozet/instant")
    (synopsis
      "A partial replacement for std::time::Instant that works on WASM too.")
    (description
      "This package provides a partial replacement for std::time::Instant that works on
WASM too.")
    (license license:bsd-3)))

(define-public rust-core-error-0.0.0
  (package
    (name "rust-core-error")
    (version "0.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core-error" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13wvc7lcpi7f6rr0racns4l52gzpix4xhih6qns30hmn5sbv5kgg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-version-check" ,rust-version-check-0.9))))
    (home-page "https://github.com/core-error/core-error")
    (synopsis "std::error::Error for libcore")
    (description "std::error::Error for libcore")
    (license (list license:expat license:asl2.0))))

(define-public rust-rhai-1
  (package
    (name "rust-rhai")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rhai" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mkgq3amzix8zcszd5sdc9z8w49pfindqgrrjd2sy67sdi6i32w9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ahash" ,rust-ahash-0.7)
         ("rust-core-error" ,rust-core-error-0.0.0)
         ("rust-instant" ,rust-instant-0.1)
         ("rust-libm" ,rust-libm-0.2)
         ("rust-no-std-compat" ,rust-no-std-compat-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rhai-codegen" ,rust-rhai-codegen-1)
         ("rust-rust-decimal" ,rust-rust-decimal-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-smartstring" ,rust-smartstring-0.2)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://rhai.rs")
    (synopsis "Embedded scripting for Rust")
    (description "Embedded scripting for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-handlebars-4
  (package
    (name "rust-handlebars")
    (version "4.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "handlebars" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1a73ja5v5bh9qd2d80fyzkbk22v56k36yy9q6hglf7ygwmjnlm15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-log" ,rust-log-0.4)
         ("rust-pest" ,rust-pest-2)
         ("rust-pest-derive" ,rust-pest-derive-2)
         ("rust-quick-error" ,rust-quick-error-2)
         ("rust-rhai" ,rust-rhai-1)
         ("rust-rust-embed" ,rust-rust-embed-6)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/sunng87/handlebars-rust")
    (synopsis "Handlebars templating implemented in Rust.")
    (description "Handlebars templating implemented in Rust.")
    (license license:expat)))

(define-public rust-gitignore-1
  (package
    (name "rust-gitignore")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gitignore" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "197rlas99iqc95fhikyspfa5flhva9pbl1jc8fn9h50ccbj91akq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-glob" ,rust-glob-0.3))))
    (home-page "https://github.com/nathankleyn/gitignore.rs")
    (synopsis
      "Implementation of .gitignore file parsing and glob testing in Rust.")
    (description
      "Implementation of .gitignore file parsing and glob testing in Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rust-stemmers-1
  (package
    (name "rust-rust-stemmers")
    (version "1.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rust-stemmers" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0m6acgdflrrcm17dj7lp7x4sfqqhga24qynv660qinwz04v20sp4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://github.com/CurrySoftware/rust-stemmers")
    (synopsis
      "A rust implementation of some popular snowball stemming algorithms")
    (description
      "This package provides a rust implementation of some popular snowball stemming
algorithms")
    (license (list license:expat license:bsd-3))))

(define-public rust-lindera-ipadic-builder-0.8
  (package
    (name "rust-lindera-ipadic-builder")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-ipadic-builder" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0r3cya3l5qsm8rciy3i6k0r1s48q61lcg79bvzaakzdqfkj5dpky"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-yada" ,rust-yada-0.5))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A Japanese morphological dictionary builder for IPADIC.")
    (description
      "This package provides a Japanese morphological dictionary builder for IPADIC.")
    (license license:expat)))

(define-public rust-lindera-ipadic-0.8
  (package
    (name "rust-lindera-ipadic")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-ipadic" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1vbwwhkqdyn4da7adafykczi6anvzckg6bngxgb0grkrc49xlvr6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-ipadic-builder" ,rust-lindera-ipadic-builder-0.8)
         ("rust-reqwest" ,rust-reqwest-0.11)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A Japanese morphological dictionary loader for IPADIC.")
    (description
      "This package provides a Japanese morphological dictionary loader for IPADIC.")
    (license license:expat)))

(define-public rust-lindera-dictionary-0.8
  (package
    (name "rust-lindera-dictionary")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-dictionary" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "122qzfkhjrivzp4vjyj709wjcwvzsnvab00jmgj519iw1z34mb38"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-lindera-core" ,rust-lindera-core-0.8))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A morphological dictionary loader.")
    (description "This package provides a morphological dictionary loader.")
    (license license:expat)))

(define-public rust-yada-0.5
  (package
    (name "rust-yada")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "yada" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hc5wl40406fi6d2ffchgxa4i034wfx4b5gdf2v2mgvvlnvjrldn"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/takuyaa/yada")
    (synopsis
      "Yada is a yet another double-array trie library aiming for fast search and compact data representation.")
    (description
      "Yada is a yet another double-array trie library aiming for fast search and
compact data representation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-lindera-core-0.8
  (package
    (name "rust-lindera-core")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera-core" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1h97pji23gpb481vk354qnqjrhnlxall6myy4ja8rsqz24s43lq9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-serde" ,rust-serde-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-yada" ,rust-yada-0.5))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A morphological analysis core library.")
    (description
      "This package provides a morphological analysis core library.")
    (license license:expat)))

(define-public rust-lindera-0.8
  (package
    (name "rust-lindera")
    (version "0.8.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "lindera" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10flm145vycvkqb2fxlg6xkiy08vriycl4sibxbyxd1ak5wpn1jf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-encoding" ,rust-encoding-0.2)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-lindera-dictionary" ,rust-lindera-dictionary-0.8)
         ("rust-lindera-ipadic" ,rust-lindera-ipadic-0.8)
         ("rust-lindera-ipadic-builder" ,rust-lindera-ipadic-builder-0.8)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/lindera-morphology/lindera")
    (synopsis "A morphological analysis library.")
    (description "This package provides a morphological analysis library.")
    (license license:expat)))

(define-public rust-cedarwood-0.4
  (package
    (name "rust-cedarwood")
    (version "0.4.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cedarwood" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0z8lipbhd0dv0msrsjd2p2p8cky8hkmkskcqincm457lz6c28cgs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-smallvec" ,rust-smallvec-1))))
    (home-page "https://github.com/MnO2/cedarwood")
    (synopsis
      "efficiently-updatable double-array trie in Rust (ported from cedar)")
    (description
      "efficiently-updatable double-array trie in Rust (ported from cedar)")
    (license unknown-license!)))

(define-public rust-jieba-rs-0.6
  (package
    (name "rust-jieba-rs")
    (version "0.6.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "jieba-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "106x4r931kwyw7s3jnqkfc4f5fj4zwna7762a3g1sh150gsi4zlc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cedarwood" ,rust-cedarwood-0.4)
         ("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-hashbrown" ,rust-hashbrown-0.11)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-ordered-float" ,rust-ordered-float-2)
         ("rust-phf" ,rust-phf-0.10)
         ("rust-phf-codegen" ,rust-phf-codegen-0.10)
         ("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/messense/jieba-rs")
    (synopsis "The Jieba Chinese Word Segmentation Implemented in Rust")
    (description "The Jieba Chinese Word Segmentation Implemented in Rust")
    (license license:expat)))

(define-public rust-elasticlunr-rs-2
  (package
    (name "rust-elasticlunr-rs")
    (version "2.3.14")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "elasticlunr-rs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s8m510cv3lf6r4h892dfmr6bdjck5wdcfza452iryq0wjdfkvk0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-jieba-rs" ,rust-jieba-rs-0.6)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lindera" ,rust-lindera-0.8)
         ("rust-lindera-core" ,rust-lindera-core-0.8)
         ("rust-regex" ,rust-regex-1)
         ("rust-rust-stemmers" ,rust-rust-stemmers-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-strum" ,rust-strum-0.21)
         ("rust-strum-macros" ,rust-strum-macros-0.21))))
    (home-page "https://github.com/mattico/elasticlunr-rs")
    (synopsis
      "A partial port of elasticlunr.js to Rust for generating static document search indexes")
    (description
      "This package provides a partial port of elasticlunr.js to Rust for generating
static document search indexes")
    (license (list license:expat license:asl2.0))))

(define-public rust-ammonia-3
  (package
    (name "rust-ammonia")
    (version "3.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ammonia" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03dw54za4zardpmkwwfp496bvwybpwf8p1yhmp0yq6kxz5d1fjxp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-html5ever" ,rust-html5ever-0.25)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-maplit" ,rust-maplit-1)
         ("rust-markup5ever-rcdom" ,rust-markup5ever-rcdom-0.1)
         ("rust-matches" ,rust-matches-0.1)
         ("rust-tendril" ,rust-tendril-0.4)
         ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/rust-ammonia/ammonia")
    (synopsis "HTML Sanitization")
    (description "HTML Sanitization")
    (license (list license:expat license:asl2.0))))

(define-public rust-mdbook-0.4
  (package
    (name "rust-mdbook")
    (version "0.4.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mdbook" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1in3xqz5i54b9bi255fwd6yyzwf5jwzl4kiv9diy1d5kgrl107r4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:tests? #f
	#:cargo-inputs
        (("rust-ammonia" ,rust-ammonia-3)
         ("rust-anyhow" ,rust-anyhow-1)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-elasticlunr-rs" ,rust-elasticlunr-rs-2)
         ("rust-env-logger" ,rust-env-logger-0.7)
         ("rust-futures-util" ,rust-futures-util-0.3)
         ("rust-gitignore" ,rust-gitignore-1)
         ("rust-handlebars" ,rust-handlebars-4)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-notify" ,rust-notify-4)
         ("rust-opener" ,rust-opener-0.5)
         ("rust-pulldown-cmark" ,rust-pulldown-cmark-0.9)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-shlex" ,rust-shlex-1)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-tokio" ,rust-tokio-1)
         ("rust-toml" ,rust-toml-0.5)
         ("rust-topological-sort" ,rust-topological-sort-0.1)
         ("rust-warp" ,rust-warp-0.3))
        #:cargo-development-inputs
        (("rust-assert-cmd" ,rust-assert-cmd-1)
         ("rust-predicates" ,rust-predicates-2)
         ("rust-pretty-assertions" ,rust-pretty-assertions-0.6)
         ("rust-select" ,rust-select-0.5)
         ("rust-semver" ,rust-semver-0.11)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/rust-lang/mdBook")
    (synopsis "Creates a book from markdown files")
    (description "Creates a book from markdown files")
    (license license:mpl2.0)))

