(define-module (yas packages hg)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix packages)
  #:use-module (ice-9 match))

(define-public python-mercurial-extension-utils
  (package
    (name "python-mercurial-extension-utils")
    (version "1.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "mercurial_extension_utils" version))
        (sha256
          (base32 "15892vn4a5sibq0cr7lhrcnj395cv5wyygsmiyq5dqdpd0l5ykg1"))))
    (build-system python-build-system)
    (inputs (list mercurial))
    (home-page "https://foss.heptapod.net/mercurial/mercurial-extension_utils")
    (synopsis "Mercurial Extension Utils")
    (description "Mercurial Extension Utils")
    (license license:bsd-3)))

(define-public python-mercurial-keyring
  (package
    (name "python-mercurial-keyring")
    (version "1.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "mercurial_keyring" version))
        (sha256
          (base32 "0a9815qv1fk2hfyz6pqww7vw6ci60mp9m0sac2chrwkx55lwlvin"))))
    (build-system python-build-system)
    (inputs (list mercurial))
    (propagated-inputs
      `(("python-keyring" ,python-keyring)
        ("python-mercurial-extension-utils"
         ,python-mercurial-extension-utils)))
    (home-page "https://foss.heptapod.net/mercurial/mercurial_keyring")
    (synopsis "Mercurial Keyring Extension")
    (description "Mercurial Keyring Extension")
    (license license:bsd-3)))

(define-public python-keyrings.cryptfile
  (package
    (name "python-keyrings.cryptfile")
    (version "1.3.8")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "keyrings.cryptfile" version))
        (sha256
          (base32 "14dgdy98rhkf23z4jz1gxdras5s06fpz9ydrgn59fylicjr9srgd"))))
    (build-system python-build-system)
    (native-inputs
      `(("python-pytest" ,python-pytest)
        ))
    (propagated-inputs
      `(("python-argon2-cffi" ,python-argon2-cffi)
        ("python-keyring" ,python-keyring)
        ("python-pycryptodome" ,python-pycryptodome)))
    (home-page "https://github.com/frispete/keyrings.cryptfile")
    (synopsis "Encrypted file keyring backend")
    (description "Encrypted file keyring backend")
    (license license:expat)))
